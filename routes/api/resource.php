<?php

use Illuminate\Support\Facades\Route;

Route::prefix('branch')->group(__DIR__.'/resource/branch.php');
Route::prefix('branch-department')->group(__DIR__.'/resource/branch-department.php');
Route::prefix('setting')->group(__DIR__.'/resource/setting.php');
Route::prefix('emp')->group(__DIR__.'/resource/emp.php');
Route::prefix('client')->group(__DIR__.'/resource/client.php');
Route::prefix('invoice')->group(__DIR__.'/resource/invoice.php');
Route::prefix('lead')->group(__DIR__.'/resource/lead.php');
Route::prefix('consultant-bonus')->group(__DIR__.'/resource/consultant-bonus.php');
Route::prefix('staff-consultant-bonus')->group(__DIR__.'/resource/staff-consultant-bonus.php');
Route::prefix('lead-consultant-bonus')->group(__DIR__.'/resource/lead-consultant-bonus.php');
// Route::prefix('service-group')->group(__DIR__.'/api/service-group.php');
