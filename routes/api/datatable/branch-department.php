<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.datatable.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.datatable.branch-department')->handle($request);
    })->name('branch-department');
});
