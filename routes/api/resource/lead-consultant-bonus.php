<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.lead-consultant-bonus.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.lead-consultant-bonus.lists')->handle($request);
    })->name('lists');
});
