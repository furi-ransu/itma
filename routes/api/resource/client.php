<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.client.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.client.lists')->handle($request);
    })->name('lists');
    Route::post('/', function (Request $request) {
        return app('app.action.api.resource.client.store')->handle($request);
    })->name('store')->middleware(['api.only.admin.can.be.access']);
    Route::put('/{id}', function (Request $request, $id) {
        return app('app.action.api.resource.client.update')->handle($request, $id);
    })->name('update')->middleware(['api.only.admin.can.be.access']);
    Route::delete('/{id}', function (Request $request, $id) {
        return app('app.action.api.resource.client.delete')->handle($request, $id);
    })->name('delete');
});
