<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.setting.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.setting.lists')->handle($request);
    })->name('lists');
    Route::post('/', function (Request $request) {
        return app('app.action.api.resource.setting.store')->handle($request);
    })->name('store')->middleware(['api.only.admin.can.be.access']);
    Route::put('/{id}', function (Request $request, $id) {
        return app('app.action.api.resource.setting.update')->handle($request, $id);
    })->name('update');
});
