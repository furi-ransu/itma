<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.staff-consultant-bonus.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.staff-consultant-bonus.lists')->handle($request);
    })->name('lists');
});
