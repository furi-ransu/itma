<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.branch-department.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.branch-department.lists')->handle($request);
    })->name('lists');
    Route::post('/', function (Request $request) {
        return app('app.action.api.resource.branch-department.store')->handle($request);
    })->name('store')->middleware(['api.only.admin.can.be.access']);
    Route::put('/{id}', function (Request $request, $id) {
        return app('app.action.api.resource.branch-department.update')->handle($request, $id);
    })->name('update')->middleware(['api.only.admin.can.be.access']);
    Route::delete('/{id}', function (Request $request, $id) {
        return app('app.action.api.resource.branch-department.delete')->handle($request, $id);
    })->name('delete');
});
