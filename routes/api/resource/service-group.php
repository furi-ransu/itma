<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.service-group.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.service-group.lists')->handle($request);
    })->name('lists');
    Route::post('/', function (Request $request) {
        return app('app.action.api.resource.service-group.store')->handle($request);
    })->name('store');
    Route::put('/{id}', function (Request $request, $id) {
        return app('app.action.api.resource.service-group.update')->handle($request, $id);
    })->name('update');
    Route::delete('/{id}', function (Request $request, $id) {
        return app('app.action.api.resource.service-group.delete')->handle($request, $id);
    })->name('delete');
});
