<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.invoice.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.invoice.lists')->handle($request);
    })->name('lists');
    Route::post('/fetch', function (Request $request) {
        return app('app.action.api.resource.invoice.fetch')->handle($request);
    })->name('fetch');
});
