<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.resource.lead.', 'middleware' => []], function () {
    Route::get('/', function (Request $request) {
        return app('app.action.api.resource.lead.lists')->handle($request);
    })->name('lists');
    Route::post('/fetch', function (Request $request) {
        return app('app.action.api.resource.lead.fetch')->handle($request);
    })->name('fetch');
});
