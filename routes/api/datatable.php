<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('city')->group(__DIR__.'/datatable/city.php');
Route::prefix('client')->group(__DIR__.'/datatable/client.php');
Route::prefix('lead')->group(__DIR__.'/datatable/lead.php');
Route::prefix('branch')->group(__DIR__.'/datatable/branch.php');
Route::prefix('invoice')->group(__DIR__.'/datatable/invoice.php');
Route::prefix('branch-department')->group(__DIR__.'/datatable/branch-department.php');
Route::prefix('emp')->group(__DIR__.'/datatable/emp.php');
Route::prefix('project')->group(__DIR__.'/datatable/project.php');
Route::group(['as' => 'api.datatable.', 'middleware' => []], function () {
    Route::get('service-category', function (Request $request) {
        return app('app.action.api.datatable.service-category')->handle($request);
    })->name('service-category');
    Route::get('service-group', function (Request $request) {
        return app('app.action.api.datatable.service-group')->handle($request);
    })->name('service-group');
    Route::get('service-item', function (Request $request) {
        return app('app.action.api.datatable.service-item')->handle($request);
    })->name('service-item');
    Route::get('team', function (Request $request) {
        return app('app.action.api.datatable.team')->handle($request);
    })->name('team');
    Route::get('team-member', function (Request $request) {
        return app('app.action.api.datatable.team-member')->handle($request);
    })->name('team-member');
});
