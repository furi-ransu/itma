<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.cms.team-member.', 'middleware' => []], function () {
    Route::post('/', function (Request $request) {
        return app('app.action.api.cms.team-member.store')->handle($request);
    })->name('store');
    Route::put('/{id}', function (Request $request, $id) {
        return app('app.action.api.cms.team-member.update')->handle($request, $id);
    })->name('update');
    Route::delete('/{id}', function (Request $request, $id) {
        return app('app.action.api.cms.team-member.delete')->handle($request, $id);
    })->name('delete');
});
