<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.cms.project.', 'middleware' => []], function () {
    Route::post('/', function (Request $request) {
        return app('app.action.api.cms.project.store')->handle($request);
    })->name('store');
});
