<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.cms.service-item.', 'middleware' => []], function () {
    Route::post('/', function (Request $request) {
        return app('app.action.api.cms.service-item.store')->handle($request);
    })->name('store');
    Route::put('/{id}', function (Request $request, $id) {
        return app('app.action.api.cms.service-item.update')->handle($request, $id);
    })->name('update');
    Route::delete('/{id}', function (Request $request, $id) {
        return app('app.action.api.cms.service-item.delete')->handle($request, $id);
    })->name('delete');
});
