<?php

use Illuminate\Support\Facades\Route;

Route::prefix('service-group')->group(__DIR__.'/cms/service-group.php');
Route::prefix('service-item')->group(__DIR__.'/cms/service-item.php');
Route::prefix('team')->group(__DIR__.'/cms/team.php');
Route::prefix('team-member')->group(__DIR__.'/cms/team-member.php');
Route::prefix('project')->group(__DIR__.'/cms/project.php');
