<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes(['register' => false]);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/otp-verification/{email}', [App\Http\Controllers\OtpVerificationController::class, 'email'])->name('otp-verification.email');
Route::post('/otp-verification', [App\Http\Controllers\OtpVerificationController::class, 'verify'])->name('otp-verification');
Route::group(['prefix' => 'cms', 'as' => 'cms.', 'middleware' => ['auth']], function () {
    Route::resource('branches', \App\Http\Controllers\Cms\BranchController::class);
    Route::resource('branch-departments', \App\Http\Controllers\Cms\BranchDepartmentController::class);
    Route::resource('settings', \App\Http\Controllers\Cms\SettingController::class);
    Route::resource('emps', \App\Http\Controllers\Cms\EmpController::class);
    Route::resource('clients', \App\Http\Controllers\Cms\ClientController::class);
    Route::resource('invoices', \App\Http\Controllers\Cms\InvoicesController::class);
    Route::resource('leads', \App\Http\Controllers\Cms\LeadController::class);
    Route::resource('consultant-bonuses', \App\Http\Controllers\Cms\ConsultantBonusController::class);
    Route::resource('staff-consultant-bonuses', \App\Http\Controllers\Cms\StaffConsultantBonusController::class);
    Route::resource('lead-consultant-bonuses', \App\Http\Controllers\Cms\LeadConsultantBonusController::class);
    Route::resource('service-groups', \App\Http\Controllers\Cms\ServiceGroupController::class);
    Route::resource('service-items', \App\Http\Controllers\Cms\ServiceItemController::class);
    Route::resource('teams', \App\Http\Controllers\Cms\TeamController::class);
    Route::resource('team-members', \App\Http\Controllers\Cms\TeamMemberController::class);
    Route::resource('projects', \App\Http\Controllers\Cms\ProjectController::class);
});
