<?php

use Illuminate\Support\Facades\Route;

Route::any('/hello', function () {
    return response()->api(200, 'success', [], 'Hello..');
});
Route::prefix('resource')->group(__DIR__.'/api/resource.php');
Route::prefix('datatable')->group(__DIR__.'/api/datatable.php');
Route::prefix('cms')->group(__DIR__.'/api/cms.php');
