<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OTP Notification</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        .notification {
            max-width: 300px;
            margin: 20px auto;
            padding: 20px;
            background-color: #ffffff;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
            border-radius: 5px;
            text-align: center;
        }

        h1 {
            color: #333;
        }

        p {
            color: #666;
        }

        .otp-code {
            font-size: 24px;
            font-weight: bold;
            margin: 10px 0;
            color: #007bff;
        }

        @media (max-width: 768px) {
            .notification {
                max-width: 80%;
            }
        }
    </style>
</head>
<body>
    <div class="notification">
        <h1>OTP Notification</h1>
        <p>Your One-Time Password (OTP) code is:</p>
        <div class="otp-code">{{ $otpCode }}</div>
        <p>Use this code for verification.</p>
    </div>
</body>
</html>
