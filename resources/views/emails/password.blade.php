<!DOCTYPE html>
<html>
  <head>
    <style>
      body {
        font-family: Arial, sans-serif;
      }
      .container {
        width: 80%;
        margin: auto;
        padding: 20px;
        border: 1px solid #ddd;
        border-radius: 5px;
        background-color: #f9f9f9;
      }
      .notification {
        padding: 10px;
        border-radius: 5px;
        background-color: #dff0d8;
        border: 1px solid #3c763d;
        margin-bottom: 10px;
      }
      .notification h4 {
        color: #3c763d;
        font-weight: bold;
      }
      .notification p {
        color: #333;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="notification">
        <h4>Hai {{ $name }} :</h4>
        <p>Your password account is : {{ $password }}</p>
      </div>
    </div>
  </body>
</html>
