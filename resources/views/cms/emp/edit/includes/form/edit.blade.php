<form id="form-update-emp" method="POST" action="{{ route('api.resource.emp.update', $id) }}">
    @method('PUT')
    <input name="user_id_logged_in" type="hidden" class="form-control" value="{{ Auth::user()->id }}" required>
    <input id="branch_department_id" name="branch_department_id" type="hidden" class="form-control" value="{{ $emp->branch_department_id }}" readonly>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm-9">
            <input name="name" type="text" class="form-control" placeholder="Name" value="{{ $emp->user->name }}" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Email</label>
        <div class="col-sm-9">
            <input name="email" type="email" class="form-control" placeholder="Email" value="{{ $emp->user->email }}" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Number</label>
        <div class="col-sm-9">
            <input name="number" type="text" class="form-control" placeholder="Number" value="{{ $emp->number }}" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Role</label>
        <div class="col-sm-9">
            <select name="role_id" class="form-select">
                @foreach ($roles as $item)
                    <option value="{{ $item->id }}" {{ $item->name == $userRole ? 'selected' : '' }}>{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Employment Status</label>
        <div class="col-sm-9">
            <select name="md_employment_status_id" class="form-select">
                @foreach ($mdEmploymentStatuses as $item)
                    <option value="{{ $item->id }}" {{ $item->id == $emp->md_employment_status_id ? 'selected' : '' }}>{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Active Status</label>
        <div class="col-sm-9">
            <select name="active_status" class="form-select">
                @foreach ($activeStatuses as $item)
                    <option value="{{ $item }}" {{ $item == $emp->user->active_status ? 'selected' : '' }}>{{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Branch</label>
        <div class="col-sm-9">
            <input value="{{ $emp->branch_department->branch->name }}" id="branch_name" name="branch_name" type="text" class="form-control" placeholder="Branch Name" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Department</label>
        <div class="col-sm-9">
            <input value="{{ $emp->branch_department->department->name }}" id="department_name" name="department_name" type="text" class="form-control" placeholder="Department Name" readonly>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.emps.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-branch-department">Select Branch Department</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
