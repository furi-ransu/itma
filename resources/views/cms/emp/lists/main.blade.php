@extends('layouts.master')

@section('title') Employee @endsection
@section('css')
    <link href="{{ URL::asset('skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('skote/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    @component('layouts.components.breadcrumb')
        @slot('li_1') Page @endslot
        @slot('title') Employee @endslot
    @endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Lists</h5>
                    <p></p>
                    @include('cms.emp.lists.includes.datatable.emp')
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="api-resource-emp-lists" value="{{ route('api.resource.emp.lists') }}"/>
@endsection
@section('script')
    <script src="{{ URL::asset('skote/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/emp/lists/datatable/emp.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/emp/lists/delete.js') }}"></script>
@endsection
