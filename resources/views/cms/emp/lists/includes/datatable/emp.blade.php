<table id="emp-datatable" class="display nowrap table table-bordered dt-responsive" style="width:100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Employment Status</th>
            <th>Branch</th>
            <th>Department</th>
            <th></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
