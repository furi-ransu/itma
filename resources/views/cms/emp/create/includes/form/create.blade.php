<form id="form-create-emp" method="POST" action="{{ route('api.resource.emp.store') }}">
    <input name="user_id_logged_in" type="hidden" class="form-control" value="{{ Auth::user()->id }}" required>
    <input id="branch_department_id" name="branch_department_id" type="hidden" class="form-control" readonly>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm-9">
            <input name="name" type="text" class="form-control" placeholder="Name" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Email</label>
        <div class="col-sm-9">
            <input name="email" type="email" class="form-control" placeholder="Email" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Number</label>
        <div class="col-sm-9">
            <input name="number" type="text" class="form-control" placeholder="Number" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Role</label>
        <div class="col-sm-9">
            <select name="role_id" class="form-select">
                @foreach ($roles as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Employment Status</label>
        <div class="col-sm-9">
            <select name="md_employment_status_id" class="form-select">
                @foreach ($mdEmploymentStatuses as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Branch</label>
        <div class="col-sm-9">
            <input id="branch_name" name="branch_name" type="text" class="form-control" placeholder="Branch Name" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Department</label>
        <div class="col-sm-9">
            <input id="department_name" name="department_name" type="text" class="form-control" placeholder="Department Name" readonly>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.emps.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-branch-department">Select Branch Department</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
