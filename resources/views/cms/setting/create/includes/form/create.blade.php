<form id="form-create-setting" method="POST" action="{{ route('api.resource.setting.store') }}">
    <input name="user_id_logged_in" type="hidden" class="form-control" value="{{ Auth::user()->id }}" required>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Key</label>
        <div class="col-sm-9">
            <input name="key" type="text" class="form-control" placeholder="Key" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Value</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="value" rows="3" placeholder="Value" required></textarea>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.settings.index') }}">Back</a>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
