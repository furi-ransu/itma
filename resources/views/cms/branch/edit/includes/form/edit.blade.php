<form id="form-update-branch" method="POST" action="{{ route('api.resource.branch.update', $branch->id) }}">
    @method('PUT')
    <input name="user_id_logged_in" type="hidden" class="form-control" value="{{ Auth::user()->id }}" required>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm-9">
            <input name="name" type="text" class="form-control" placeholder="Name" value="{{ $branch->name }}" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">City</label>
        <div class="col-sm-9">
            <input id="city_name" type="text" class="form-control" value="{{ $branch->city->name }}" readonly required>
            <input id="city_id" name="city_id" type="hidden" value="{{ $branch->city->id }}" class="form-control">
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.branches.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-city">Select City</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
