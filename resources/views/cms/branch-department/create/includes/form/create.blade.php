<form id="form-create-branch-department" method="POST" action="{{ route('api.resource.branch-department.store') }}">
    <input name="user_id_logged_in" type="hidden" class="form-control" value="{{ Auth::user()->id }}" required>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Branch Name</label>
        <div class="col-sm-9">
            <input id="branch_name" name="branch_name" type="text" class="form-control" placeholder="Branch Name" required readonly>
            <input id="branch_id" name="branch_id" type="hidden" class="form-control" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Department</label>
        <div class="col-sm-9">
            <select name="department_id" class="form-select">
                @foreach ($departments as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.branch-departments.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-branch">Select Branch</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
