<table id="branch-department-datatable" class="display nowrap table table-bordered dt-responsive" style="width:100%">
    <thead>
        <tr>
            <th>Branch</th>
            <th>Department</th>
            <th>City</th>
            <th>State</th>
            <th>Country</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
