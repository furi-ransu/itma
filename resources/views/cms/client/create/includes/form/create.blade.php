<form id="form-create-client" method="POST" action="{{ route('api.resource.client.store') }}">
    <input name="user_id_logged_in" type="hidden" class="form-control" value="{{ Auth::user()->id }}" required>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm-9">
            <input name="name" type="text" class="form-control" placeholder="Name" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Phone Number</label>
        <div class="col-sm-9">
            <input name="phone_number" type="text" class="form-control" placeholder="Phone Number" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Email</label>
        <div class="col-sm-9">
            <input name="email" type="email" class="form-control" placeholder="Email" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">KYC Url Document</label>
        <div class="col-sm-9">
            <input name="kyc_url_document" type="text" class="form-control" placeholder="KYC Url Document" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">KYC Status</label>
        <div class="col-sm-9">
            <select name="md_kyc_status_id" class="form-select">
                @foreach ($kycStatuses as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Type</label>
        <div class="col-sm-9">
            <select name="type" class="form-select">
                @foreach ($types as $item)
                    <option value="{{ $item }}">{{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">City</label>
        <div class="col-sm-9">
            <input id="city_name" type="text" class="form-control" readonly required>
            <input id="city_id" name="city_id" type="hidden" class="form-control">
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Address</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="address" rows="3" placeholder="Address" required></textarea>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.clients.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-city">Select City</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
