<table id="client-datatable" class="display nowrap table table-bordered dt-responsive" style="width:100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>KYC Status</th>
            <th>City</th>
            <th>State</th>
            <th>Country</th>
            <th></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
