<form id="form-create-invoice" method="POST" action="{{ route('api.resource.invoice.fetch') }}">
    <input name="user_id_logged_in" type="hidden" class="form-control" value="{{ Auth::user()->id }}" required>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Customer Number</label>
        <div class="col-sm-9">
            <input name="customer_number" type="text" class="form-control" placeholder="Customer Number" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Year</label>
        <div class="col-sm-9">
            <input name="year" type="text" class="form-control" placeholder="Year" required>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.invoices.index') }}">Back</a>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
