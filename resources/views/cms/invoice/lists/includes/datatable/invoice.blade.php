<table id="invoice-datatable" class="display nowrap table table-bordered dt-responsive" style="width:100%">
    <thead>
        <tr>
            <th>Number</th>
            <th>Transaction Date</th>
            <th>Due Date</th>
            <th>Customer Name</th>
            <th>Customer Number</th>
            <th>Currency Iso Code</th>
            <th>Status</th>
            <th>Nominal</th>
            <th>Age of Unpaid Invoice</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
