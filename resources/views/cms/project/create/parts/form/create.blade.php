<form id="form-create-project" method="POST" action="{{ route('api.cms.project.store') }}">
    <input id="lead_id" name="lead_id" type="hidden" class="form-control">
    <input id="invoice_id" name="invoice_id" type="hidden" class="form-control">
    <input id="client_id" name="client_id" type="hidden" class="form-control">
    <input id="branch_department_id" name="branch_department_id" type="hidden" class="form-control">
    <input id="service_item_id" name="service_item_id" type="hidden" class="form-control">
    <input id="user_id" name="user_id" type="hidden" class="form-control" value="{{ auth()->user()->id }} }}">
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Number</label>
        <div class="col-sm-9">
            <input name="number" type="text" class="form-control" placeholder="number" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Lead</label>
        <div class="col-sm-9">
            <input id="lead_name" name="lead_name" type="text" class="form-control" placeholder="Lead" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Invoice</label>
        <div class="col-sm-9">
            <input id="invoice_number" name="invoice_number" type="text" class="form-control" placeholder="Invoice Number" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Client</label>
        <div class="col-sm-9">
            <input id="client_name" name="client_name" type="text" class="form-control" placeholder="Client" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Branch</label>
        <div class="col-sm-9">
            <input id="branch_name" name="branch_name" type="text" class="form-control" placeholder="Branch" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Department</label>
        <div class="col-sm-9">
            <input id="department_name" name="department_name" type="text" class="form-control" placeholder="Department" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Service Item</label>
        <div class="col-sm-9">
            <input id="service_item_name" name="service_item_name" type="text" class="form-control" placeholder="Service Iteam" readonly>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Service Category</label>
        <div class="col-md-9">
            <select name="service_category_id" class="form-select">
                <option value="1">One Time</option>
                <option value="2">Recurring</option>
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Project Type</label>
        <div class="col-md-9">
            <select name="project_type_id" class="form-select">
                @foreach ($projectTypes as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.projects.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-branch-department">Select Branch Department</button>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-service-item">Select Service Item</button>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-client">Select Client</button>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-lead">Select Lead</button>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-invoice">Select Invoice</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
