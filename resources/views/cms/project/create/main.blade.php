@extends('layouts.master')
@section('title') Project Plan @endsection
@section('css')
    <link href="{{ URL::asset('skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('skote/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    @component('layouts.components.breadcrumb')
        @slot('li_1') Page @endslot
        @slot('title') Project Plan @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Create</h4>
                    @include('cms.project.create.parts.form.create')
                </div>
            </div>
        </div>
    </div>
    @include('cms.project.create.parts.datatable.modal-table-branch-department')
    <input type="hidden" id="api-datatable-branch-department" value="{{ route('api.datatable.branch-department') }}"/>
    @include('cms.project.create.parts.datatable.modal-table-service-item')
    <input type="hidden" id="api-datatable-service-item" value="{{ route('api.datatable.service-item') }}"/>
    @include('cms.project.create.parts.datatable.modal-table-client')
    <input type="hidden" id="api-datatable-client" value="{{ route('api.datatable.client') }}"/>
    @include('cms.project.create.parts.datatable.modal-table-lead')
    <input type="hidden" id="api-datatable-lead" value="{{ route('api.datatable.lead') }}"/>
    @include('cms.project.create.parts.datatable.modal-table-invoice')
    <input type="hidden" id="api-datatable-invoice" value="{{ route('api.datatable.invoice') }}"/>
@endsection
@section('script')
    <script src="{{ URL::asset('skote/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/project/create/branch-department-datatable.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/project/create/service-item-datatable.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/project/create/client-datatable.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/project/create/lead-datatable.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/project/create/invoice-datatable.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/project/create/store.js') }}"></script>
@endsection
