<table id="project-datatables" class="display nowrap table table-bordered dt-responsive" style="width:100%">
    <thead>
        <tr>
            <th>Number</th>
            <th>Client</th>
            <th>Lead</th>
            <th>Invoice</th>
            <th>Branch</th>
            <th>Department</th>
            <th>Service Item</th>
            <th>Service Category</th>
            <th>Project Type</th>
            <th>Project Status</th>
            <th>Consultant</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
