<form id="form-edit-team-member" method="POST" action="{{ route('api.cms.team-member.update', $id) }}">
    @method('PUT')
    <input id="employee_id" name="employee_id" type="hidden" class="form-control" value="{{ $teamMember->employee->id }}">
    <input id="team_id" name="team_id" type="hidden" class="form-control" value="{{ $teamMember->team->id }}">
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Employee Name</label>
        <div class="col-sm-9">
            <input id="employee_name" type="text" class="form-control" value="{{ $teamMember->employee->user->name }}" readonly required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Team Name</label>
        <div class="col-sm-9">
            <input id="team_name" type="text" class="form-control" readonly required value="{{ $teamMember->team->name }}">
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.team-members.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-employee">Select Employee</button>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target="#modal-table-team">Select Team</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
