@extends('layouts.master')
@section('title') Team Member @endsection
@section('css')
    <link href="{{ URL::asset('skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('skote/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    @component('layouts.components.breadcrumb')
        @slot('li_1') Page @endslot
        @slot('title') Team Member @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Create</h4>
                    @include('cms.team-member.create.parts.form.create')
                </div>
            </div>
        </div>
    </div>
    @include('cms.team-member.create.parts.datatable.modal.table-team')
    <input type="hidden" id="api-datatable-team" value="{{ route('api.datatable.team') }}"/>
    @include('cms.team-member.create.parts.datatable.modal.table-emp-occupation')
    <input type="hidden" id="api-datatable-emp" value="{{ route('api.datatable.emp') }}"/>
@endsection
@section('script')
    <script src="{{ URL::asset('skote/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('skote/libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/team-member/create/team-datatable.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/team-member/create/emp-datatable.js') }}"></script>
    <script src="{{ URL::asset('app/js/cms/team-member/create/store.js') }}"></script>
@endsection
