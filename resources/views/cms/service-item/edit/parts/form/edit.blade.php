<form id="form-edit-service-item" method="POST" action="{{ route('api.cms.service-item.update', $id) }}">
    @method('PUT')
    <input id="service_group_id" name="service_group_id" type="hidden" value="{{ $serviceItem->service_group->id }}" class="form-control">
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm-9">
            <input name="name" type="text" class="form-control" placeholder="Name" value="{{ $serviceItem->name }}" required>
        </div>
    </div>
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">Service Group</label>
        <div class="col-sm-9">
            <input id="service_group_name" type="text" class="form-control" value="{{ $serviceItem->service_group->name }}" readonly required>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.service-items.index') }}">Back</a>
                <button type="button" class="btn btn-secondary w-md" data-bs-toggle="modal" data-bs-target=".bs-example-modal-lg">Select Service Group</button>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
