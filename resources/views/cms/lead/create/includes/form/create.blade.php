<form id="form-create-lead" method="POST" action="{{ route('api.resource.lead.fetch') }}">
    <div class="row mb-4">
        <label class="col-sm-3 col-form-label">ID</label>
        <div class="col-sm-9">
            <input name="id" type="text" class="form-control" placeholder="Id" required>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-sm-9">
            <div>
                <a class="btn btn-dark w-md" href="{{ route('cms.leads.index') }}">Back</a>
                <button type="submit" class="btn btn-primary w-md">Save</button>
            </div>
        </div>
    </div>
    <div style="display: none" class="spinner-border text-primary m-1" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</form>
