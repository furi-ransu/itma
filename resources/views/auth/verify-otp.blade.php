@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(count($errors) > 0)
                @foreach( $errors->all() as $message )
                <div class="alert alert-danger small" role="alert">
                        <strong> Error! </strong> {{ $message }}
                    </div>
                @endforeach
            @endif
            <div class="card">
                <div class="card-header">{{ __('OTP Verify') }}</div>
                <div class="card-body">
                    {{ html()->form('POST', route('otp-verification'))->open() }}
                        <input type="hidden" name="email" value="{{ $email }}">
                        <div class="row mb-3">
                            <label for="mobile_no" class="col-md-4 col-form-label text-md-end">OTP Code</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="code" required autofocus placeholder="Enter Your OTP Code" maxLength="6">
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button class="btn btn-primary w-md" type="submit">Verify</button>
                            </div>
                        </div>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
