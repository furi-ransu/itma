<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-street-view"></i>
        <span key="t-client">{{ __('Client') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.clients.index')  }}" key="t-service-groups-level-1">Lists</a></li>
        <li><a href="{{ route('cms.clients.create')  }}" key="t-service-groups-level-1-2">Create</a></li>
    </ul>
</li>
