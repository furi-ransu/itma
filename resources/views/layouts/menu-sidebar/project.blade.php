<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-aperture"></i>
        <span key="t-project">{{ __('Project') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.projects.index')  }}" key="t-projects-level-1">Lists</a></li>
        <li><a href="{{ route('cms.projects.create')  }}" key="t-projects-level-1-2">Create</a></li>
    </ul>
</li>
