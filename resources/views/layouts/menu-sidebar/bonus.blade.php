<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-money"></i>
        <span key="t-bonus-consultant">{{ __('Bonus Consultant') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.staff-consultant-bonuses.index') }}" key="t-bonus-consultants-staff-level-1">Staff</a></li>
        <li><a href="{{ route('cms.lead-consultant-bonuses.index') }}" key="t-bonus-consultants-lead-level-1">Team Leader</a></li>
    </ul>
</li>
