<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-buildings"></i>
        <span key="t-branch">{{ __('Branch') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.branches.index')  }}" key="t-branches-level-1">Lists</a></li>
        <li><a href="{{ route('cms.branches.create')  }}" key="t-branches-level-1-2">Create</a></li>
    </ul>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-building"></i>
        <span key="t-branch">{{ __('Branch Department') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.branch-departments.index')  }}" key="t-branch-departments-level-1">Lists</a></li>
        <li><a href="{{ route('cms.branch-departments.create')  }}" key="t-branch-departments-level-1-2">Create</a></li>
    </ul>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-male"></i>
        <span key="t-emp">{{ __('Employee') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.emps.index')  }}" key="t-emps-level-1">Lists</a></li>
        <li><a href="{{ route('cms.emps.create')  }}" key="t-emps-level-1-2">Create</a></li>
    </ul>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-layer"></i>
        <span key="t-service">{{ __('Service') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li>
            <a href="javascript: void(0);" class="has-arrow" key="t-level-1-2">{{ __('Service Group') }}</a>
            <ul class="sub-menu" aria-expanded="true">
                <li><a href="{{ route('cms.service-groups.index')  }}" key="t-service-groups-level-1">Lists</a></li>
                <li><a href="{{ route('cms.service-groups.create')  }}" key="t-service-groups-level-1-2">Create</a></li>
            </ul>
        </li>
        <li>
            <a href="javascript: void(0);" class="has-arrow" key="t-level-1-2">{{ __('Service Item') }}</a>
            <ul class="sub-menu" aria-expanded="true">
                <li><a href="{{ route('cms.service-items.index')  }}" key="t-service-items-level-1">Lists</a></li>
                <li><a href="{{ route('cms.service-items.create')  }}" key="t-service-items-level-1-2">Create</a></li>
            </ul>
        </li>
    </ul>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-run"></i>
        <span key="t-team">{{ __('Team') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.teams.index')  }}" key="t-teams-level-1">Lists</a></li>
        <li><a href="{{ route('cms.teams.create')  }}" key="t-teams-level-1-2">Create</a></li>
    </ul>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-body"></i>
        <span key="t-team-member">{{ __('Team Member') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.team-members.index')  }}" key="t-team-members-level-1">Lists</a></li>
        <li><a href="{{ route('cms.team-members.create')  }}" key="t-team-members-level-1-2">Create</a></li>
    </ul>
</li>
