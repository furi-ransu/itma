<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-file-blank"></i>
        <span key="t-invoice">{{ __('Lead') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.leads.index')  }}" key="t-leads-level-1">Lists</a></li>
        <li><a href="{{ route('cms.leads.create')  }}" key="t-leads-level-1-2">Get</a></li>
    </ul>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-dollar-circle"></i>
        <span key="t-invoice">{{ __('Invoice') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.invoices.index')  }}" key="t-invoices-level-1">Lists</a></li>
        <li><a href="{{ route('cms.invoices.create')  }}" key="t-invoices-level-1-2">Get</a></li>
    </ul>
</li>
