<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="bx bx-wrench"></i>
        <span key="t-setting">{{ __('Setting') }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        <li><a href="{{ route('cms.settings.index')  }}" key="t-settings-level-1">Lists</a></li>
        <li><a href="{{ route('cms.settings.create')  }}" key="t-settings-level-1-2">Create</a></li>
    </ul>
</li>
