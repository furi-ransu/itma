<!-- JAVASCRIPT -->
<script src="{{ URL::asset('skote/libs/jquery/jquery.min.js')}}"></script>
<script src="{{ URL::asset('skote/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ URL::asset('skote/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{ URL::asset('skote/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{ URL::asset('skote/libs/node-waves/waves.min.js')}}"></script>
@yield('script')

<!-- App js -->
<script src="{{ URL::asset('skote/js/app.js')}}"></script>

@yield('script-bottom')
