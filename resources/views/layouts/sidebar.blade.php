<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span key="t-dashboard">Dashboard</span>
                    </a>
                </li>
                <li class="menu-title" key="t-company">{{ __('Company') }}</li>
                @include('layouts.menu-sidebar.company')
                <li class="menu-title" key="t-client">{{ __('Client') }}</li>
                @include('layouts.menu-sidebar.client')
                <li class="menu-title" key="t-third-party">{{ __('3rd Party') }}</li>
                @include('layouts.menu-sidebar.third-party')
                <li class="menu-title" key="t-project">{{ __('Project') }}</li>
                @include('layouts.menu-sidebar.project')
                <li class="menu-title" key="t-bonus">{{ __('Bonus') }}</li>
                @include('layouts.menu-sidebar.bonus')
                <li class="menu-title" key="t-setting">{{ __('Setting') }}</li>
                @include('layouts.menu-sidebar.setting')
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
