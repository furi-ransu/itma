<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MdKycStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'verified',
            'pending',
            'rejected',
        ];
        foreach ($data as $key => $value) {
            \App\Models\MdKycStatus::create(['name' => $value]);
        }
    }
}
