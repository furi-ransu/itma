<?php

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\Department;
use Illuminate\Database\Seeder;
use App\Models\BranchDepartment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class BranchDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $branch = Branch::create([
            'name' => 'jakarta selatan',
            'city_id' => 56724,
        ]);
        $department = Department::where('name', 'it')->first();
        BranchDepartment::create([
            'branch_id' => $branch->id,
            'department_id' => $department->id,
        ]);
    }
}
