<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(RoleSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableChunkOneSeeder::class);
        $this->call(CitiesTableChunkTwoSeeder::class);
        $this->call(CitiesTableChunkThreeSeeder::class);
        $this->call(CitiesTableChunkFourSeeder::class);
        $this->call(CitiesTableChunkFiveSeeder::class);
        $this->call(MdEmploymentStatusSeeder::class);
        $this->call(MdKycStatusSeeder::class);
        $this->call(CurrenciesSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(BranchDepartmentSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(ServiceCategorySeeder::class);
        $this->call(MdDayTypeSeeder::class);
        $this->call(MdDeadlineDayTypeSeeder::class);
        $this->call(ProjectStatusSeeder::class);
        $this->call(ProjectTypeSeeder::class);
    }
}
