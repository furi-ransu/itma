<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'new',
            'renewal',
        ];

        foreach ($data as $key => $value) {
            \App\Models\ProjectType::create([
                'name' => $value
            ]);
        }
    }
}
