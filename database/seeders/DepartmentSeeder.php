<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $departments = [
            'consultant',
            'tax',
            'business advisory',
            'gms',
            'legal delivery',
            'external finance',
            'amt',
            'it'
        ];

        foreach ($departments as $key => $value) {
            DB::table('departments')->insert([
                'name' => $value,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
