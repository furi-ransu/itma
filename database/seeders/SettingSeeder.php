<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $keys = [
            'accurate_access_token' => '0',
            'accurate_refresh_token' => '0',
            'accurate_scope' => '0',
            'accurate_code' => '0',
            'accurate_session_id' => '0',
            'usd_to_usd' => 1,
            'idr_to_usd' => '0.00007142857',
            'sgd_to_usd' => '0.74',
            'bonus_dept_consultant_tier_1_total_net_renewal_value_benefit_percentage' => 1,
            'bonus_dept_consultant_tier_1_total_net_renewal_value_threshold_usd' => 0,
            'bonus_dept_consultant_tier_2_total_net_new_value_benefit_percentage' => 1,
            'bonus_dept_consultant_tier_2_total_net_new_value_threshold_usd' => 5000,
            'bonus_dept_consultant_tier_3_total_net_new_value_benefit_percentage' => 1,
            'bonus_dept_consultant_tier_3_total_net_new_value_threshold_usd' => 14000,
            'bonus_dept_consultant_tier_4_net_new_value_above_target_benefit_percentage' => 1,
            'bonus_dept_consultant_tier_4_net_new_value_above_target_threshold_usd' => 14000,
            'bonus_dept_consultant_tier_5_net_new_value_difference_previous_month_benefit_percentage' => 1,
            'bonus_dept_consultant_tier_5_net_new_value_difference_previous_month_threshold_usd' => 14000,
            'bonus_dept_consultant_total_team_tier_1_benefit_percentage' => 0.15,
            'bonus_dept_consultant_total_team_tier_1_treshold_usd' => 4000,
            'bonus_dept_consultant_total_team_tier_2_benefit_percentage' => 0.4,
            'bonus_dept_consultant_total_team_tier_2_treshold_usd' => 14000,
            'bonus_dept_consultant_age_of_invoice_in_day' => 37,
            'project_additional_days_of_payment_deferral_in_day' => 67,
        ];

        foreach ($keys as $key => $value) {
            \App\Models\Setting::create([
                'key' => $key,
                'value' => $value
            ]);
        }
    }
}
