<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MdDeadlineDayType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MdDeadlineDayTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MdDeadlineDayType::create([
            'name' => 'back day',
        ]);
        MdDeadlineDayType::create([
            'name' => 'forward day',
        ]);
    }
}
