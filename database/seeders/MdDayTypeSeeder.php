<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MdDayTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'working day',
            'calendar day',
            'statutory day',
        ];

        foreach ($data as $key => $value) {
            \App\Models\MdDayType::firstOrCreate(['name' => $value]);
        }
    }
}
