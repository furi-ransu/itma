<?php

namespace Database\Seeders;

use App\Models\Emp;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->billy();
        $this->daris();
        $this->bagir();
    }

    private function billy()
    {
        $branch = \App\Models\Branch::where('name', 'jakarta selatan')->first();
        $department = \App\Models\Department::where('name', 'it')->first();
        $branchDepartment = \App\Models\BranchDepartment::where('branch_id', $branch->id)->where('department_id', $department->id)->first();

        $billy = \App\Models\User::factory()->create([
            'name' => 'billy',
            'email' => 'billy@incorp.co.id',
            'password' => bcrypt('pokemon'),
        ]);
        $billy->assignRole('admin');
        Emp::create([
            'number' => '0001',
            'user_id' => \App\Models\User::where('email', 'billy@incorp.co.id')->first()->id,
            'md_employment_status_id' => 3,
            'branch_department_id' => $branchDepartment->id,
        ]);
    }

    private function daris()
    {
        $branch = \App\Models\Branch::where('name', 'jakarta selatan')->first();
        $department = \App\Models\Department::where('name', 'it')->first();
        $branchDepartment = \App\Models\BranchDepartment::where('branch_id', $branch->id)->where('department_id', $department->id)->first();

        $daris = \App\Models\User::factory()->create([
            'name' => 'daris salam',
            'email' => 'daris.salam@incorp.co.id',
            'password' => bcrypt('pokemon'),
        ]);
        $daris->assignRole('admin');
        Emp::create([
            'number' => '0002',
            'user_id' => \App\Models\User::where('email', 'daris.salam@incorp.co.id')->first()->id,
            'md_employment_status_id' => 3,
            'branch_department_id' => $branchDepartment->id,
        ]);
    }

    private function bagir()
    {
        $branch = \App\Models\Branch::where('name', 'jakarta selatan')->first();
        $department = \App\Models\Department::where('name', 'it')->first();
        $branchDepartment = \App\Models\BranchDepartment::where('branch_id', $branch->id)->where('department_id', $department->id)->first();

        $bagir = \App\Models\User::factory()->create([
            'name' => 'muchammad bagir',
            'email' => 'muchammad.bagir@incorp.co.id',
            'password' => bcrypt('pokemon'),
        ]);
        $bagir->assignRole('admin');
        Emp::create([
            'number' => '0003',
            'user_id' => \App\Models\User::where('email', 'muchammad.bagir@incorp.co.id')->first()->id,
            'md_employment_status_id' => 3,
            'branch_department_id' => $branchDepartment->id,
        ]);
    }
}
