<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MdEmploymentStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MdEmploymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MdEmploymentStatus::create([
            'name' => 'contract',
        ]);
        MdEmploymentStatus::create([
            'name' => 'probation',
        ]);
        MdEmploymentStatus::create([
            'name' => 'permanent',
        ]);
    }
}
