<?php

namespace Database\Seeders;

use App\Models\ProjectStatus;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProjectStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            'created',
            'has been assignment',
            'on progress',
            'done',
            'hold',
            'sign off'
        ];

        foreach ($data as $key => $value) {
            ProjectStatus::create(['name' => $value]);
        }
    }
}
