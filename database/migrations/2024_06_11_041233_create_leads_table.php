<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('date');
            $table->text('description');
            $table->enum('is_won', ['yes', 'no'])->default('no');
            $table->string('odoo_lead_id');
            $table->string('odoo_partner_id');
            $table->string('odoo_partner_name')->nullable();
            $table->string('odoo_partner_email')->nullable();
            $table->string('odoo_partner_phone_number')->nullable();
            $table->json('detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leads');
    }
};
