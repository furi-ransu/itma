<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('number')->unique();
            $table->string('po_number')->nullable();
            $table->string('tax_number')->nullable();
            $table->date('due_date')->nullable();
            $table->date('transaction_date');
            $table->string('customer_name');
            $table->string('customer_number');
            $table->string('currency_iso_code')->nullable();
            $table->string('status');
            $table->decimal('nominal', 15, 2);
            $table->smallInteger('age_of_unpaid_invoice')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invoices');
    }
};
