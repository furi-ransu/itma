<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pic_cbs', function (Blueprint $table) {
            $table->id();
            $table->decimal('total_nominal', 15, 2)->default(0.00);
            $table->foreignId('emp_id');
            $table->foreignId('team_leader_id')->nullable();
            $table->foreignId('currency_id');
            $table->foreignId('md_employment_status_id');
            $table->foreignId('consultant_bonus_id');
            $table->timestamps();
            $table->foreign('emp_id')->references('id')->on('emps')->onDelete('cascade');
            $table->foreign('team_leader_id')->references('id')->on('emps')->onDelete('cascade');
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade');
            $table->foreign('md_employment_status_id')->references('id')->on('md_employment_statuses')->onDelete('cascade');
            $table->foreign('consultant_bonus_id')->references('id')->on('consultant_bonuses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pic_cbs');
    }
};
