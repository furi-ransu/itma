<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('work_items', function (Blueprint $table) {
            $table->id();
            $table->string('pipeline');
            $table->smallInteger('timeline');
            $table->tinyInteger('order')->default(1);
            $table->foreignId('md_day_type_id');
            $table->foreignId('service_item_id');
            $table->timestamps();
            $table->foreign('md_day_type_id')->references('id')->on('md_day_types')->onDelete('cascade');
            $table->foreign('service_item_id')->references('id')->on('service_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('work_items');
    }
};
