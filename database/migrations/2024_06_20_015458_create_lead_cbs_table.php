<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lead_cbs', function (Blueprint $table) {
            $table->id();
            $table->decimal('total_nominal', 15, 2)->default(0.00);
            $table->foreignId('currency_id');
            $table->foreignId('emp_id');
            $table->foreignId('consultant_bonus_id');
            $table->timestamps();
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('emp_id')->references('id')->on('emps');
            $table->foreign('consultant_bonus_id')->references('id')->on('consultant_bonuses');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lead_cbs');
    }
};
