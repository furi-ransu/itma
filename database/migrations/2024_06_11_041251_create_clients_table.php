<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('active_status', ['yes', 'no'])->default('yes');
            $table->enum('type', ['individual', 'corporate'])->default('individual');
            $table->string('email')->unique();
            $table->string('address');
            $table->string('phone_number')->unique();
            $table->string('kyc_url_document')->nullable();
            $table->foreignId('md_kyc_status_id');
            $table->foreignId('city_id');
            $table->timestamps();
            $table->foreign('md_kyc_status_id')->references('id')->on('md_kyc_statuses')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clients');
    }
};
