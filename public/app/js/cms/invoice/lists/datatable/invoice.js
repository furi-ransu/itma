$(document).ready(function() {
    $('#invoice-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-resource-invoice-lists').value,
        columns: [{
                data: 'number',
                name: 'number'
            },
            {
                data: 'transaction_date',
                name: 'transaction_date'
            },
            {
                data: 'due_date',
                name: 'due_date'
            },
            {
                data: 'customer_name',
                name: 'customer_name'
            },
            {
                data: 'customer_number',
                name: 'customer_number'
            },
            {
                data: 'currency_iso_code',
                name: 'currency_iso_code'
            },
            {
                data: 'status',
                name: 'status'
            },
            {
                data: 'nominal',
                name: 'nominal'
            },
            {
                data: 'age_of_unpaid_invoice',
                name: 'age_of_unpaid_invoice'
            },
        ]
    });
});
