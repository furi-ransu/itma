$(document).ready(function() {
	var table = $('#city-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-datatable-city').value,
		columns: [
			{
				data: 'name',
				name: 'name'
			},
            {
				data: 'state.name',
				name: 'state.name'
			},
			{
				data: 'state.country.name',
				name: 'state.country.name'
			},
        ]
	});

    $('#city-datatable tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#city_id').val(data.id);
        $('#city_name').val(data.name);
        alert('You choose ' + data.name);
    });
});
