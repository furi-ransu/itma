$('#branch-datatable').on('click', '.btn-danger', function() {
    var url = $(this).data('url')
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
    }).then((result) => {
        if (result.isConfirmed) {
            fetch(url, {
                method: 'DELETE',
            })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(data => {
                        throw new Error(data.message)
                    });
                }
            })
            .then(data => {
                $('#branch-datatable').DataTable().ajax.reload();
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: data.message,
                });
            })
            .catch(error => {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: error,
                });
            });
        }
    });
});
