$(document).ready(function() {
	$('#branch-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-resource-branch-lists').value,
		columns: [
            {
				data: 'name',
				name: 'name'
			},
			{
				data: 'city.name',
				name: 'city.name'
			},
            {
				data: 'city.state.name',
				name: 'city.state.name'
			},
			{
				data: 'city.state.country.name',
				name: 'city.state.country.name'
			},
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
		]
	});
});
