$(document).ready(function() {
	$('#branch-department-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-resource-list-branch-department').value,
		columns: [
			{
				data: 'branch.name',
				name: 'branch.name'
			},
            {
				data: 'department.name',
				name: 'department.name'
			},
			{
				data: 'branch.city.name',
				name: 'branch.city.name'
			},
			{
				data: 'branch.city.state.name',
				name: 'branch.city.state.name'
			},
			{
				data: 'branch.city.state.country.name',
				name: 'branch.city.state.country.name'
			},
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
		]
	});
});
