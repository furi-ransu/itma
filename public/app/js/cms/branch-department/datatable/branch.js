$(document).ready(function() {
	var table = $('#branch-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api.datatable.branch').value,
		columns: [
			{
				data: 'name',
				name: 'name'
			},
            {
				data: 'city.name',
				name: 'city.name'
			},
            {
				data: 'city.state.name',
				name: 'city.state.name'
			},
			{
				data: 'city.state.country.name',
				name: 'city.state.country.name'
			},
        ]
	});

    $('#branch-datatable tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#branch_id').val(data.id);
        $('#branch_name').val(data.name);
        alert('You choose ' + data.name);
    });
});
