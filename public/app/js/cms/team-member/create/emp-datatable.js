$(document).ready(function () {
    var table = $('#emp-occupation-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-emp').value,
        columns: [
            { data: 'user.name', name: 'user.name' },
            { data: 'branch_department.department.name', name: 'branch_department.department.name' },
            { data: 'branch_department.branch.name', name: 'branch_department.branch.name' },
        ]
    });

    $('#emp-occupation-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#employee_name').val(data.user.name);
        $('#emp_id').val(data.id);
        alert('You choose employee ' + data.user.name);
    });
});
