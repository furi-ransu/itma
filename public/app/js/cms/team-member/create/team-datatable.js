$(document).ready(function () {
    var table = $('#team-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-team').value,
        columns: [
            { data: 'name', name: 'name' },
            { data: 'branch_department.branch.name', name: 'branch_department.branch.name' },
            { data: 'branch_department.department.name', name: 'branch_department.department.name' },
        ]
    });

    $('#team-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#team_id').val(data.id);
        $('#team_name').val(data.name);
        $('#branch_name').val(data.branch_department.branch.name);
        $('#department_name').val(data.branch_department.department.name);
        alert('You choose team ' + data.name);
    });
});
