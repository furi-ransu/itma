$(document).ready(function () {
    var table = $('#employee-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-cms-datatable-employee').value,
        columns: [
            { data: 'user.name', name: 'user.name' },
            { data: 'department.name', name: 'department.name' },
            { data: 'department.branch.name', name: 'department.branch.name' },
        ]
    });

    $('#employee-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#employee_id').val(data.id);
        $('#employee_name').val(data.user.name);
        alert('You choose employee ' + data.user.name);
    });
});
