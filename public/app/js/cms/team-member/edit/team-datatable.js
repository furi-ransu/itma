$(document).ready(function () {
    var table = $('#team-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-cms-datatable-team').value,
        columns: [
            { data: 'name', name: 'name' },
            { data: 'department.name', name: 'department.name' },
            { data: 'department.branch.name', name: 'department.branch.name' },
        ]
    });

    $('#team-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#team_id').val(data.id);
        $('#team_name').val(data.name);
        alert('You choose team ' + data.name);
    });
});
