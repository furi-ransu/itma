$(document).ready(function() {
    $('#team-member-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-company-team-member').value,
        columns: [{
                data: 'team.name',
                name: 'team.name'
            },
            {
                data: 'emp.user.name',
                name: 'emp.user.name'
            },
            {
                data: 'team.branch_department.branch.name',
                name: 'team.branch_department.branch.name'
            },
            {
                data: 'team.branch_department.department.name',
                name: 'team.branch_department.department.name'
            },
        ]
    });

    $('#team-member-datatables').on('click', '.btn-danger', function() {
        var url = $(this).data('url')
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(url, {
                        method: 'DELETE',
                    })
                    .then(response => {
                        if (response.ok) {
                            return response.json();
                        } else {
                            return response.json().then(data => {
                                throw new Error(data.message)
                            });
                        }
                    })
                    .then(data => {
                        $('#team-member-datatables').DataTable().ajax.reload();
                        Swal.fire({
                            icon: "success",
                            title: "Success",
                            text: data.message,
                        });
                    })
                    .catch(error => {
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: error,
                        });
                    });
            }
        });
    });
});
