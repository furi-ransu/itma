$(document).ready(function() {
	$('#consultant-bonus-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-resource-consultant-bonus-lists').value,
		columns: [
            {
				data: 'month',
				name: 'month'
			},
			{
				data: 'year',
				name: 'year'
			},
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
		]
	});
});
