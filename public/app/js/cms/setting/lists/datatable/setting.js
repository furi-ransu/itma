$(document).ready(function() {
	$('#setting-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-resource-setting-lists').value,
		columns: [
            {
				data: 'key',
				name: 'key'
			},
			{
				data: 'value',
				name: 'value'
			},
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
		]
	});
});
