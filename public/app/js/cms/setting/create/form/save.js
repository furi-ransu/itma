$(document).ready(function () {
    const form = document.getElementById('form-create-setting');
    const spinner = document.querySelector('.spinner-border');
    form.addEventListener('submit', (event) => {
        event.preventDefault(); // Prevent form submission
        spinner.style.display = 'inline-block';
        const formData = new FormData(form);
        const formAction = form.action;
        fetch(formAction, {
            method: 'POST',
            body: formData
        })
        .then(response => {
            spinner.style.display = 'none';
            if (response.ok) {
                return response.json();
            } else {
                return response.json().then(data => {
                    throw new Error(data.message)
                });
            }
        })
        .then(data => {
            Swal.fire({
                icon: "success",
                title: "Success",
                text: data.message,
            });
            form.reset()
        })
        .catch(error => {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: error,
            });
        });
    });
});
