$(document).ready(function() {
    $('#lead-datatable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-resource-lead-lists').value,
        columns: [{
                data: 'name',
                name: 'name'
            },
            {
                data: 'date',
                name: 'date'
            },
            {
                data: 'is_won',
                name: 'is_won'
            },
            {
                data: 'odoo_lead_id',
                name: 'odoo_lead_id'
            },
            {
                data: 'odoo_partner_id',
                name: 'odoo_partner_id'
            },
            {
                data: 'odoo_partner_name',
                name: 'odoo_partner_name'
            },
            {
                data: 'odoo_partner_email',
                name: 'odoo_partner_email'
            },
        ]
    });
});
