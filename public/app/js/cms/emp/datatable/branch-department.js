$(document).ready(function() {
	var table = $('#branch-department-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-datatable-branch-department').value,
		columns: [
			{
				data: 'branch.name',
				name: 'branch.name'
			},
            {
				data: 'department.name',
				name: 'department.name'
			},
            {
				data: 'branch.city.name',
				name: 'branch.city.name'
			},
            {
				data: 'branch.city.state.name',
				name: 'branch.city.state.name'
			},
			{
				data: 'branch.city.state.country.name',
				name: 'branch.city.state.country.name'
			},
        ]
	});

    $('#branch-department-datatable tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#branch_department_id').val(data.id);
        $('#branch_name').val(data.branch.name);
        $('#department_name').val(data.department.name);
        alert('You choose branch' + data.branch.name + ' and department ' + data.department.name);
    });
});
