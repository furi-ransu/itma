$(document).ready(function() {
	$('#emp-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-resource-emp-lists').value,
		columns: [
            {
				data: 'user.name',
				name: 'user.name'
			},
            {
				data: 'user.email',
				name: 'user.email'
			},
            {
				data: 'role_name',
				name: 'role_name'
			},
            {
				data: 'md_employment_status.name',
				name: 'md_employment_status.name'
			},
			{
				data: 'branch_department.branch.name',
				name: 'branch_department.branch.name'
			},
            {
				data: 'branch_department.department.name',
				name: 'branch_department.department.name'
			},
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
		]
	});
});
