$(document).ready(function() {
    $('#project-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-project').value,
        columns: [{
                data: 'number',
                name: 'number'
            },
            {
                data: 'client.name',
                name: 'client.name'
            },
            {
                data: 'lead.name',
                name: 'lead.name'
            },
            {
                data: 'invoice.number',
                name: 'invoice.number'
            },
            {
                data: 'branch_department.branch.name',
                name: 'branch_department.branch.name'
            },
            {
                data: 'branch_department.department.name',
                name: 'branch_department.department.name'
            },
            {
                data: 'service_item.name',
                name: 'service_item.name'
            },
            {
                data: 'service_category.name',
                name: 'service_category.name'
            },
            {
                data: 'project_type.name',
                name: 'project_type.name'
            },
            {
                data: 'project_status.name',
                name: 'project_status.name'
            },
            {
                data: 'emp.user.name',
                name: 'emp.user.name'
            },
        ]
    });
});
