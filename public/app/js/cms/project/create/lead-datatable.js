$(document).ready(function () {
    var table = $('#lead-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-lead').value,
        columns: [
            { data: 'name', name: 'name' },
            { data: 'odoo_partner_name', name: 'odoo_partner_name' },
            { data: 'is_won', name: 'is_won' },
        ]
    });

    $('#lead-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#lead_id').val(data.id);
        $('#lead_name').val(data.name);
        alert('You choose lead ' + data.name);
    });
});
