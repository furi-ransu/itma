$(document).ready(function () {
    var table = $('#service-item-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-service-item').value,
        columns: [
            { data: 'name', name: 'name' },
            { data: 'service_group.name', name: 'service_group.name' }
        ]
    });

    $('#service-item-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#service_item_id').val(data.id);
        $('#service_item_name').val(data.name);
        alert('You choose service ' + data.name);
    });
});
