$(document).ready(function () {
    var table = $('#invoice-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-invoice').value,
        columns: [
            { data: 'number', name: 'number' },
            { data: 'status', name: 'status' },
            { data: 'nominal', name: 'nominal' },
            { data: 'currency_iso_code', name: 'currency_iso_code' },
            { data: 'customer_name', name: 'customer_name' },
            { data: 'transaction_date', name: 'transaction_date' },
        ]
    });

    $('#invoice-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#invoice_id').val(data.id);
        $('#invoice_number').val(data.number);
        alert('You choose number ' + data.number);
    });
});
