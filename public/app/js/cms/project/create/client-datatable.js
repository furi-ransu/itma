$(document).ready(function () {
    var table = $('#client-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-client').value,
        columns: [
            { data: 'name', name: 'name' },
        ]
    });

    $('#client-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#client_id').val(data.id);
        $('#client_name').val(data.name);
        alert('You choose client ' + data.name);
    });
});
