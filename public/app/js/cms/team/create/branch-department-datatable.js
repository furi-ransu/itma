$(document).ready(function () {
    var table = $('#branch-department-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-branch-department').value,
        columns: [
            { data: 'department.name', name: 'department.name' },
            { data: 'branch.name', name: 'branch.name' }
        ]
    });

    $('#branch-department-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#branch_department_id').val(data.id);
        $('#department_name').val(data.department.name);
        $('#branch_name').val(data.branch.name);
        alert('You choose department ' + data.department.name);
    });
});
