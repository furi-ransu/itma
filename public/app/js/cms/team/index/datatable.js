$(document).ready(function() {
    $('#team-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-datatable-company-team').value,
        columns: [{
                data: 'name',
                name: 'name'
            },
            {
                data: 'branch_department.branch.name',
                name: 'branch_department.branch.name'
            },
            {
                data: 'branch_department.department.name',
                name: 'branch_department.department.name'
            }
        ]
    });
    $('#team-datatables').on('click', '.btn-danger', function() {
        var url = $(this).data('url')
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(url, {
                        method: 'DELETE',
                    })
                    .then(response => {
                        if (response.ok) {
                            return response.json();
                        } else {
                            return response.json().then(data => {
                                throw new Error(data.message)
                            });
                        }
                    })
                    .then(data => {
                        $('#team-datatables').DataTable().ajax.reload();
                        Swal.fire({
                            icon: "success",
                            title: "Success",
                            text: data.message,
                        });
                    })
                    .catch(error => {
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: error,
                        });
                    });
            }
        });
    });
});
