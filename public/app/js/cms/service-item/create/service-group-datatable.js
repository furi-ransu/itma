$(document).ready(function () {
    var table = $('#service-group-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-cms-datatable-service-group').value,
        columns: [
            { data: 'name', name: 'name' }
        ]
    });

    $('#service-group-datatables tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        $('#service_group_id').val(data.id);
        $('#service_group_name').val(data.name);
        alert('You choose ' + data.name);
    });
});
