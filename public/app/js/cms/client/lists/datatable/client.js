$(document).ready(function() {
	$('#client-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-resource-client-lists').value,
		columns: [
            {
				data: 'name',
				name: 'name'
			},
			{
				data: 'email',
				name: 'email'
			},
            {
				data: 'phone_number',
				name: 'phone_number'
			},
            {
				data: 'md_kyc_status.name',
				name: 'md_kyc_status.name'
			},
            {
				data: 'city.name',
				name: 'city.name'
			},
            {
				data: 'city.state.name',
				name: 'city.state.name'
			},
			{
				data: 'city.state.country.name',
				name: 'city.state.country.name'
			},
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
		]
	});
});
