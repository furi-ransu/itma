$(document).ready(function() {
    $('#service-group-datatables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: document.getElementById('api-cms-datatable-service-group').value,
        columns: [{
                data: 'name',
                name: 'name'
            },
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
        ]
    });

    $('#service-group-datatables').on('click', '.btn-danger', function() {
        var url = $(this).data('url')
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(url, {
                        method: 'DELETE',
                    })
                    .then(response => {
                        if (response.ok) {
                            return response.json();
                        } else {
                            return response.json().then(data => {
                                throw new Error(data.message)
                            });
                        }
                    })
                    .then(data => {
                        $('#service-group-datatables').DataTable().ajax.reload();
                        Swal.fire({
                            icon: "success",
                            title: "Success",
                            text: data.message,
                        });
                    })
                    .catch(error => {
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: error,
                        });
                    });
            }
        });
    });
});
