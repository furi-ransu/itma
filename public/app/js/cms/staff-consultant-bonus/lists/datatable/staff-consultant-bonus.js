$(document).ready(function() {
	$('#staff-consultant-bonus-datatable').DataTable({
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: document.getElementById('api-resource-staff-consultant-bonus-lists').value,
		columns: [
            {
				data: 'emp.user.name',
				name: 'emp.user.name'
			},
            {
				data: 'total_nominal',
				name: 'total_nominal'
			},
            {
				data: 'currency.name',
				name: 'currency.name'
			},
            {
				data: 'md_employment_status.name',
				name: 'md_employment_status.name'
			},
		]
	});
});
