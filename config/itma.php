<?php

return [
    'itma_odoo' => [
        'url' => env('ITMA_ODOO_URL'),
    ],
    'accurate' => [
        'oauth_token' => env('ACCURATE_OAUTH_TOKEN'),
        'client_id' => env('ACCURATE_CLIENT_ID'),
        'client_secret' => env('ACCURATE_CLIENT_SECRET'),
        'public_url' => env('ACCURATE_PUBLIC_URL'),
    ],
];
