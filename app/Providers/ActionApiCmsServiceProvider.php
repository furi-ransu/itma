<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ActionApiCmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->serviceGroup();
        $this->serviceItem();
        $this->team();
        $this->teamMember();
        $this->project();
    }

    public function serviceGroup(): void
    {
        $this->app->bind(
            'app.action.api.cms.service-group.store',
            \App\Actions\Api\Cms\ServiceGroup\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.service-group.update',
            \App\Actions\Api\Cms\ServiceGroup\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.service-group.delete',
            \App\Actions\Api\Cms\ServiceGroup\Delete\Handler::class
        );
    }

    public function serviceItem(): void
    {
        $this->app->bind(
            'app.action.api.cms.service-item.store',
            \App\Actions\Api\Cms\ServiceItem\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.service-item.update',
            \App\Actions\Api\Cms\ServiceItem\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.service-item.delete',
            \App\Actions\Api\Cms\ServiceItem\Delete\Handler::class
        );
    }

    public function team(): void
    {
        $this->app->bind(
            'app.action.api.cms.team.store',
            \App\Actions\Api\Cms\Team\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.team.update',
            \App\Actions\Api\Cms\Team\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.team.delete',
            \App\Actions\Api\Cms\Team\Delete\Handler::class
        );
    }

    public function teamMember(): void
    {
        $this->app->bind(
            'app.action.api.cms.team-member.store',
            \App\Actions\Api\Cms\TeamMember\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.team-member.update',
            \App\Actions\Api\Cms\TeamMember\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.cms.team-member.delete',
            \App\Actions\Api\Cms\TeamMember\Delete\Handler::class
        );
    }

    public function project(): void
    {
        $this->app->bind(
            'app.action.api.cms.project.store',
            \App\Actions\Api\Cms\Project\Store\Handler::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
