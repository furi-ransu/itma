<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ActionApiDatatableProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(
            'app.action.api.datatable.city',
            \App\Actions\Api\Datatable\City\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.branch',
            \App\Actions\Api\Datatable\Branch\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.branch-department',
            \App\Actions\Api\Datatable\BranchDepartment\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.emp',
            \App\Actions\Api\Datatable\Emp\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.project',
            \App\Actions\Api\Datatable\Project\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.client',
            \App\Actions\Api\Datatable\Client\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.lead',
            \App\Actions\Api\Datatable\Lead\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.invoice',
            \App\Actions\Api\Datatable\Invoice\Handler::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
