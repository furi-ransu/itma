<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ActionApiDatatableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(
            'app.action.api.datatable.service-category',
            \App\Actions\Api\Datatable\ServiceCategory\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.service-group',
            \App\Actions\Api\Datatable\ServiceGroup\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.service-item',
            \App\Actions\Api\Datatable\ServiceItem\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.team',
            \App\Actions\Api\Datatable\Team\Handler::class
        );

        $this->app->bind(
            'app.action.api.datatable.team-member',
            \App\Actions\Api\Datatable\TeamMember\Handler::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
