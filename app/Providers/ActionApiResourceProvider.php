<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ActionApiResourceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->branch();
        $this->branchDepartment();
        $this->setting();
        $this->emp();
        $this->client();
        $this->invoice();
        $this->lead();
        $this->consultantBonus();
        $this->staffConsultantBonus();
        $this->leadConsultantBonus();
        $this->serviceGroup();
    }

    public function branch()
    {
        $this->app->bind(
            'app.action.api.resource.branch.lists',
            \App\Actions\Api\Resource\Branch\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.branch.store',
            \App\Actions\Api\Resource\Branch\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.branch.update',
            \App\Actions\Api\Resource\Branch\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.branch.delete',
            \App\Actions\Api\Resource\Branch\Delete\Handler::class
        );
    }

    public function branchDepartment()
    {
        $this->app->bind(
            'app.action.api.resource.branch-department.lists',
            \App\Actions\Api\Resource\BranchDepartment\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.branch-department.store',
            \App\Actions\Api\Resource\BranchDepartment\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.branch-department.update',
            \App\Actions\Api\Resource\BranchDepartment\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.branch-department.delete',
            \App\Actions\Api\Resource\BranchDepartment\Delete\Handler::class
        );
    }

    public function setting()
    {
        $this->app->bind(
            'app.action.api.resource.setting.lists',
            \App\Actions\Api\Resource\Setting\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.setting.store',
            \App\Actions\Api\Resource\Setting\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.setting.update',
            \App\Actions\Api\Resource\Setting\Update\Handler::class
        );
    }

    public function emp()
    {
        $this->app->bind(
            'app.action.api.resource.emp.lists',
            \App\Actions\Api\Resource\Emp\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.emp.store',
            \App\Actions\Api\Resource\Emp\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.emp.update',
            \App\Actions\Api\Resource\Emp\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.emp.delete',
            \App\Actions\Api\Resource\Emp\Delete\Handler::class
        );
    }

    public function client()
    {
        $this->app->bind(
            'app.action.api.resource.client.lists',
            \App\Actions\Api\Resource\Client\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.client.store',
            \App\Actions\Api\Resource\Client\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.client.update',
            \App\Actions\Api\Resource\Client\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.client.delete',
            \App\Actions\Api\Resource\Client\Delete\Handler::class
        );
    }

    public function invoice(): void
    {
        $this->app->bind(
            'app.action.api.resource.invoice.lists',
            \App\Actions\Api\Resource\Invoice\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.invoice.fetch',
            \App\Actions\Api\Resource\Invoice\Fetch\Handler::class
        );
    }

    public function lead(): void
    {
        $this->app->bind(
            'app.action.api.resource.lead.lists',
            \App\Actions\Api\Resource\Lead\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.lead.fetch',
            \App\Actions\Api\Resource\Lead\Fetch\Handler::class
        );
    }

    public function consultantBonus(): void
    {
        $this->app->bind(
            'app.action.api.resource.consultant-bonus.lists',
            \App\Actions\Api\Resource\ConsultantBonus\Lists\Handler::class
        );
    }

    public function staffConsultantBonus(): void
    {
        $this->app->bind(
            'app.action.api.resource.staff-consultant-bonus.lists',
            \App\Actions\Api\Resource\StaffConsultantBonus\Lists\Handler::class
        );
    }

    public function leadConsultantBonus(): void
    {
        $this->app->bind(
            'app.action.api.resource.lead-consultant-bonus.lists',
            \App\Actions\Api\Resource\LeadConsultantBonus\Lists\Handler::class
        );
    }

    public function serviceGroup(): void
    {
        $this->app->bind(
            'app.action.api.resource.service-group.lists',
            \App\Actions\Api\Resource\ServiceGroup\Lists\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.service-group.store',
            \App\Actions\Api\Resource\ServiceGroup\Store\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.service-group.update',
            \App\Actions\Api\Resource\ServiceGroup\Update\Handler::class
        );

        $this->app->bind(
            'app.action.api.resource.service-group.delete',
            \App\Actions\Api\Resource\ServiceGroup\Delete\Handler::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
