<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ActionWebUiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(
            'app.action.webui.otp.verify',
            \App\Actions\WebUi\Otp\Verify\Handler::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
