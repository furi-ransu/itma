<?php

namespace App\Console\Commands;

use App\Enums\ProjectTypeEnum;
use App\Models\CliErrorLog;
use App\Models\ConsultantBonus;
use App\Models\Currency;
use App\Models\Emp;
use App\Models\PicCb;
use App\Models\Project;
use App\Models\ProjectCb;
use App\Models\Setting;
use App\Models\Team;
use App\Models\TeamMember;
use Carbon\CarbonImmutable;
use DB;
use Illuminate\Console\Command;

class GenerateConsultantBonusStaff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:generate-consultant-bonus-staff';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            $now = now();
            $month = $now->month;
            $year = $now->year;
            $consultantBonusExists = $this->checkConsultantBonusExist($month, $year);

            if ($consultantBonusExists == true) {
                DB::commit();

                return 0;
            }

            // save to table consultant_bonuses
            $consultantBonusCreated = ConsultantBonus::create([
                'month' => $month,
                'year' => $year,
            ]);

            $rangeDate = $this->getRangeDate($month, $year);
            $listsProjectByRangeDate = $this->getProjectByRangeDate($rangeDate['start'], $rangeDate['end']);

            if (count($listsProjectByRangeDate) < 1) {
                DB::commit();

                return 0;
            }
            // save to table project_cbs
            foreach ($listsProjectByRangeDate as $key => $value) {
                ProjectCb::create([
                    'project_id' => $value['id'],
                    'consultant_bonus_id' => $consultantBonusCreated->id,
                ]);
            }

            $groupingProjectsByConsultantIds = $this->groupingProjectsByConsultantId($listsProjectByRangeDate);
            $groupingProjectsByConsultantIdWithTeamLeader = $this->groupingProjectsByConsultantIdWithTeamLeaderAndSummary($groupingProjectsByConsultantIds);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBefore = $this->getBonusBefore($groupingProjectsByConsultantIdWithTeamLeader);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforRenewal = $this->summaryProjectTypeRenewal($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBefore);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeNewUpsell = $this->summaryProjectTypeNewUpsell($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforRenewal);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier1 = $this->getBonusTier1($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeNewUpsell);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier2 = $this->getBonusTier2($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier1);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier3 = $this->getBonusTier3($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier2);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier4 = $this->getBonusTier4($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier3);
            $groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier5 = $this->getBonusTier5($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier4);

            // save to table pic_cbs
            foreach ($groupingProjectsByConsultantIdWithTeamLeaderWithBonusBeforeTier5 as $key => $value) {
                PicCb::create([
                    'total_nominal' => array_sum([$value['bonus_tier_1'], $value['bonus_tier_2'], $value['bonus_tier_3'], $value['bonus_tier_4'], $value['bonus_tier_5']]),
                    'emp_id' => $value['consultant_id'],
                    'team_leader_id' => $value['team_leader_id'],
                    'consultant_bonus_id' => $consultantBonusCreated->id,
                    'currency_id' => Currency::where('iso_code', 'USD')->first()->id,
                    'md_employment_status_id' => Emp::where('id', $value['consultant_id'])->first()->md_employment_status_id,
                ]);
            }
            DB::commit();
        } catch (\Exeption $e) {
            DB::rollBack();
            CliErrorLog::create([
                'message' => $e->getMessage(),
                'signature' => 'app:generate-consultant-bonus-staff',
            ]);
        }
    }

    public function checkConsultantBonusExist($month, $year)
    {
        return ConsultantBonus::where('month', $month)->where('year', $year)->exists();
    }

    public function getRangeDate($month, $year)
    {
        $currentDate = CarbonImmutable::now();
        $bonusConsultantAgeInvoice = Setting::where('key', 'bonus_dept_consultant_age_of_invoice_in_day')->first();

        // Add n days to the current date
        $datePlusNDays = $currentDate->addDays((int) $bonusConsultantAgeInvoice->value);
        // Subtract n days from the current date
        $dateMinusNDays = $currentDate->subDays((int) $bonusConsultantAgeInvoice->value);

        return [
            'start' => $dateMinusNDays->format('Y-m-d'),
            'end' => $datePlusNDays->format('Y-m-d'),
        ];
    }

    public function getProjectByRangeDate($start, $end)
    {
        $projects = Project::with(['invoice'])->doesntHave('project_cbs')
            ->whereHas('invoice', function ($query) use ($start, $end) {
                $query->whereBetween('transaction_date', [$start, $end]);
            })
            ->get()
            ->toArray();

        return $projects;
    }

    public function groupingProjectsByConsultantId($projects)
    {
        $result = [];
        $grouped = [];
        foreach ($projects as $item) {
            $grouped[$item['consultant_id']][] = $item;
        }

        foreach ($grouped as $key => $value) {
            $i = 0;
            $projects = $value;
            $result[$i] = [
                'consultant_id' => $key,
            ];
            foreach ($projects as $keyV => $valueV) {
                $result[$i]['projects'][] = $valueV['id'];
                $result[$i]['nominals'][] = convertToUsd($valueV['invoice']['currency_iso_code'], $valueV['invoice']['nominal']);
            }
            $i++;
        }

        return $result;
    }

    public function groupingProjectsByConsultantIdWithTeamLeaderAndSummary($groupingProjectsByConsultantIds)
    {
        foreach ($groupingProjectsByConsultantIds as $key => $value) {
            $totalNominal = array_sum($value['nominals']);
            $groupingProjectsByConsultantIds[$key]['summary_nominal'] = $totalNominal;
        }

        foreach ($groupingProjectsByConsultantIds as $key => $value) {
            $teamMember = TeamMember::where('emp_id', $value['consultant_id'])->first();
            $team = Team::where('id', $teamMember->team_id)->first();
            $teamLeaderId = $team->present('default')->get_team_leader_id;
            $groupingProjectsByConsultantIds[$key]['team_leader_id'] = $teamLeaderId;
        }

        return $groupingProjectsByConsultantIds;
    }

    public function getBonusBefore($groupingProjectsByConsultantIdWithTeamLeader)
    {
        $now = now();
        $previousMonth = $now->subMonth();
        $monthPrevious = $previousMonth->month;
        $year = $previousMonth->year;
        $consultantBonusPrevious = ConsultantBonus::where('month', $monthPrevious)->where('year', $year)->first();

        if (is_null($consultantBonusPrevious)) {
            foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
                $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_before'] = 0;
            }
        } else {
            foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
                $picCbBefore = PicCb::where('consultant_bonus_id', $consultantBonusPrevious->id)->where('emp_id', $value['consultant_id'])->first();
                if ($picCbBefore) {
                    $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_before'] = $picCbBefore->total_nominal;
                } else {
                    $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_before'] = 0;
                }
            }
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }

    public function summaryProjectTypeRenewal($groupingProjectsByConsultantIdWithTeamLeader)
    {
        foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
            $renewal = [];
            $projects = $value['projects'];
            foreach ($projects as $keyV => $valueV) {
                $project = Project::with(['invoice', 'project_type'])->where('id', $valueV)->first();
                if ($project->project_type->name == ProjectTypeEnum::RENEWAL()->getValue()) {
                    $renewal[] = $value['nominals'][$keyV];
                }
            }
            $groupingProjectsByConsultantIdWithTeamLeader[$key]['summary_renewal'] = array_sum($renewal);
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }

    public function summaryProjectTypeNewUpsell($groupingProjectsByConsultantIdWithTeamLeader)
    {
        foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
            $newUpsell = [];
            $projects = $value['projects'];
            foreach ($projects as $keyV => $valueV) {
                $project = Project::with(['invoice', 'project_type'])->where('id', $valueV)->first();
                if ($project->project_type->name == ProjectTypeEnum::NEW()->getValue()) {
                    $newUpsell[] = $value['nominals'][$keyV];
                }
            }
            $groupingProjectsByConsultantIdWithTeamLeader[$key]['summary_new_upsell'] = array_sum($newUpsell);
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }

    public function getBonusTier1($groupingProjectsByConsultantIdWithTeamLeader)
    {
        $setting = Setting::where('key', 'bonus_dept_consultant_tier_1_total_net_renewal_value_benefit_percentage')->first();
        foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
            $bonusTier1 = (($setting->value / 100) * $value['summary_renewal']);
            $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_1'] = $bonusTier1;
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }

    public function getBonusTier2($groupingProjectsByConsultantIdWithTeamLeader)
    {
        $settingPercentage = Setting::where('key', 'bonus_dept_consultant_tier_2_total_net_new_value_benefit_percentage')->first();
        $settingTreshold = Setting::where('key', 'bonus_dept_consultant_tier_2_total_net_new_value_threshold_usd')->first();
        foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
            if ($value['summary_new_upsell'] > $settingTreshold->value) {
                $bonusTier2 = (($settingPercentage->value / 100) * $value['summary_new_upsell']);
                $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_2'] = $bonusTier2;
            } else {
                $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_2'] = 0;
            }
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }

    public function getBonusTier3($groupingProjectsByConsultantIdWithTeamLeader)
    {
        $settingPercentage = Setting::where('key', 'bonus_dept_consultant_tier_3_total_net_new_value_benefit_percentage')->first();
        $settingTreshold = Setting::where('key', 'bonus_dept_consultant_tier_3_total_net_new_value_threshold_usd')->first();
        foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
            if ($value['summary_new_upsell'] > $settingTreshold->value) {
                $bonusTier3 = (($settingPercentage->value / 100) * $value['summary_new_upsell']);
                $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_3'] = $bonusTier3;
            } else {
                $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_3'] = 0;
            }
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }

    public function getBonusTier4($groupingProjectsByConsultantIdWithTeamLeader)
    {
        $settingPercentage = Setting::where('key', 'bonus_dept_consultant_tier_3_total_net_new_value_benefit_percentage')->first();
        $settingTreshold = Setting::where('key', 'bonus_dept_consultant_tier_3_total_net_new_value_threshold_usd')->first();
        foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
            if ($value['summary_new_upsell'] > $settingTreshold->value) {
                $substract = $value['summary_new_upsell'] - $settingTreshold->value;
                $bonusTier4 = (($settingPercentage->value / 100) * $substract);
                $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_4'] = $bonusTier4;
            } else {
                $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_4'] = 0;
            }
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }

    public function getBonusTier5($groupingProjectsByConsultantIdWithTeamLeader)
    {
        $settingPercentage = Setting::where('key', 'bonus_dept_consultant_tier_3_total_net_new_value_benefit_percentage')->first();
        foreach ($groupingProjectsByConsultantIdWithTeamLeader as $key => $value) {
            $treshold = $value['bonus_before'];
            if ($treshold == 0) {
                $bonusTier5 = 0;
            } elseif ($value['summary_new_upsell'] > $treshold) {
                $substract = $value['summary_new_upsell'] - $treshold;
                $bonusTier5 = (($settingPercentage->value / 100) * $substract);
            } else {
                $bonusTier5 = 0;
            }
            $groupingProjectsByConsultantIdWithTeamLeader[$key]['bonus_tier_5'] = $bonusTier5;
        }

        return $groupingProjectsByConsultantIdWithTeamLeader;
    }
}
