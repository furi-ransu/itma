<?php

namespace App\Console\Commands;

use App\Models\CliErrorLog;
use App\Models\ConsultantBonus;
use App\Models\Currency;
use App\Models\LeadCb;
use App\Models\PicCb;
use App\Models\Setting;
use DB;
use Illuminate\Console\Command;

class GenerateConsultantBonusTeamLeader extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:generate-consultant-bonus-team-leader';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            $now = now();
            $month = $now->month;
            $year = $now->year;

            $consultantBonusExists = $this->checkConsultantBonusExist($month, $year);
            if ($consultantBonusExists == false) {
                DB::commit();

                return 0;
            }

            $consultantBonus = ConsultantBonus::where('month', $month)->where('year', $year)->first();
            $leadConsultantBonusExists = LeadCb::where('consultant_bonus_id', $consultantBonus->id)->get();
            if ($leadConsultantBonusExists->count() > 0) {
                DB::commit();

                return 0;
            }

            $leadConsultantBonuses = PicCb::whereNotNull('team_leader_id')->get();
            if ($leadConsultantBonuses->count() == 0) {
                DB::commit();

                return 0;
            }

            $leadConsultantBonusesGroupByTeamLeaderIds = $this->groupByTeamLeaderId($leadConsultantBonuses->toArray());
            $leadConsultantBonusesGroupByTeamLeaderIdsRemoveNullTotalNominal = $this->removeNullTotalNominal($leadConsultantBonusesGroupByTeamLeaderIds);
            $leadConsultantBonusesGroupByTeamLeaderTier1 = $this->getBonusTier1($leadConsultantBonusesGroupByTeamLeaderIdsRemoveNullTotalNominal);
            $leadConsultantBonusesGroupByTeamLeaderTier2 = $this->getBonusTier2($leadConsultantBonusesGroupByTeamLeaderTier1);
            $leadConsultantBonusesGroupByTeamLeaderTotalTier = $this->getBonusTotalTier($leadConsultantBonusesGroupByTeamLeaderTier2);

            foreach ($leadConsultantBonusesGroupByTeamLeaderTotalTier as $key => $value) {
                LeadCb::create([
                    'total_nominal' => $value['total_tier'],
                    'currency_id' => Currency::where('iso_code', 'USD')->first()->id,
                    'emp_id' => $value['team_leader_id'],
                    'consultant_bonus_id' => $consultantBonus->id,
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            CliErrorLog::create([
                'message' => $e->getMessage(),
                'signature' => 'app:generate-consultant-bonus-team-leader',
            ]);
        }
    }

    public function checkConsultantBonusExist($month, $year)
    {
        return ConsultantBonus::where('month', $month)->where('year', $year)->exists();
    }

    public function groupByTeamLeaderId($leadConsultantBonuses)
    {
        $result = [];
        $grouped = [];
        foreach ($leadConsultantBonuses as $item) {
            $grouped[$item['team_leader_id']][] = $item;
        }

        foreach ($grouped as $key => $value) {
            $i = 0;
            $bonusPics = $value;
            $result[$i] = [
                'team_leader_id' => $key,
                'total_nominal' => [],
            ];
            foreach ($bonusPics as $keyV => $valueV) {
                $empId = $valueV['emp_id'];
                $emp = \App\Models\Emp::with(['md_employment_status'])->where('id', $empId)->first();
                if ($emp->md_employment_status->name != 'probation') {
                    $result[$i]['total_nominal'][] = convertToUsd('USD', $valueV['total_nominal']);
                }
            }
            $i++;
        }

        return $result;
    }

    public function removeNullTotalNominal($leadConsultantBonusesGroupByTeamLeaderIds)
    {
        $result = [];

        foreach ($leadConsultantBonusesGroupByTeamLeaderIds as $key => $value) {
            $totalNominals = count($value['total_nominal']);
            if ($totalNominals > 0) {
                $result[$key] = [
                    'team_leader_id' => $value['team_leader_id'],
                    'total_nominal' => $value['total_nominal'],
                ];
            }
        }

        return $result;
    }

    public function getBonusTier1($leadConsultantBonusesGroupByTeamLeaderIds)
    {
        foreach ($leadConsultantBonusesGroupByTeamLeaderIds as $key => $value) {
            $totalNominals = count($value['total_nominal']);
            $sumNominals = array_sum($value['total_nominal']);
            $treshold = Setting::where('key', 'bonus_dept_consultant_total_team_tier_1_treshold_usd')->first();
            if ($sumNominals > $treshold->value) {
                $leadConsultantBonusesGroupByTeamLeaderIds[$key]['tier_1'] = ($totalNominals * $treshold->value);
            } else {
                $leadConsultantBonusesGroupByTeamLeaderIds[$key]['tier_1'] = 0;
            }
        }

        return $leadConsultantBonusesGroupByTeamLeaderIds;
    }

    public function getBonusTier2($leadConsultantBonusesGroupByTeamLeaderIds)
    {
        foreach ($leadConsultantBonusesGroupByTeamLeaderIds as $key => $value) {
            $totalNominals = count($value['total_nominal']);
            $sumNominals = array_sum($value['total_nominal']);
            $treshold = Setting::where('key', 'bonus_dept_consultant_total_team_tier_2_treshold_usd')->first();
            if ($sumNominals > $treshold->value) {
                $leadConsultantBonusesGroupByTeamLeaderIds[$key]['tier_2'] = ($totalNominals * $treshold->value);
            } else {
                $leadConsultantBonusesGroupByTeamLeaderIds[$key]['tier_2'] = 0;
            }
        }

        return $leadConsultantBonusesGroupByTeamLeaderIds;
    }

    public function getBonusTotalTier($leadConsultantBonusesGroupByTeamLeaderIds)
    {
        foreach ($leadConsultantBonusesGroupByTeamLeaderIds as $key => $value) {
            $totalTier = $value['tier_1'] + $value['tier_2'];
            $percentage1 = Setting::where('key', 'bonus_dept_consultant_total_team_tier_1_benefit_percentage')->first();
            $treshold1 = Setting::where('key', 'bonus_dept_consultant_total_team_tier_1_treshold_usd')->first();
            $percentage2 = Setting::where('key', 'bonus_dept_consultant_total_team_tier_2_benefit_percentage')->first();
            $treshold2 = Setting::where('key', 'bonus_dept_consultant_total_team_tier_2_treshold_usd')->first();

            if ($totalTier > $treshold1->value) {
                $leadConsultantBonusesGroupByTeamLeaderIds[$key]['total_tier'] = ($totalTier * $percentage1->value);
            } elseif ($totalTier > $treshold2->value) {
                $leadConsultantBonusesGroupByTeamLeaderIds[$key]['total_tier'] = ($totalTier * $percentage2->value);
            } else {
                $leadConsultantBonusesGroupByTeamLeaderIds[$key]['total_tier'] = 0;
            }
        }

        return $leadConsultantBonusesGroupByTeamLeaderIds;
    }
}
