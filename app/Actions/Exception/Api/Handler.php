<?php

namespace App\Actions\Exception\Api;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler
{
    public function handle(Throwable $e): array
    {
        $detailError = [];

        if ($e instanceof ValidationException == true) {
            $detailError = self::validationException($e);
        } elseif ($e instanceof QueryException == true) {
            $detailError = self::queryException($e);
        } elseif ($e instanceof ClientException == true) {
            $detailError = self::clientException($e);
        } else {
            $detailError = self::elseException($e);
        }

        return $detailError;
    }

    public static function elseException(Throwable $e): array
    {
        $action = '';
        $searchKeyOfError = null;
        $exceptionCode = $e->getCode();
        $exceptionMessage = $e->getMessage();
        $appErrorMessages = [];

        if (count($appErrorMessages) > 0) {
            foreach ($appErrorMessages as $key => $value) {
                if ($value['message'] === $exceptionMessage) {
                    $searchKeyOfError = $key;
                }
            }
        }

        if (is_string($searchKeyOfError)) {
            $code = $appErrorMessages[$searchKeyOfError]['code'];
            $message = $appErrorMessages[$searchKeyOfError]['message'];
            $action = $appErrorMessages[$searchKeyOfError]['action'];
        } else {
            $message = $exceptionMessage;
            $code = (is_numeric($exceptionCode) == true) ? $exceptionCode : 500;
            $code = ($code == 0) ? 500 : $code;
            $action = '';
        }

        return compact('code', 'message', 'action');
    }

    public static function validationException(Throwable $e): array
    {
        $errors = $e->errors();
        if (empty($errors)) {
            $message = 'an unexpected error occurred';
        } else {
            foreach ($errors as $key => $value) {
                $message = $value[0];
            }
        }
        $code = 400;
        $action = '';

        return compact('code', 'message', 'action');
    }

    public static function queryException(Throwable $e): array
    {
        $message = (config('app.env') == 'production') ? 'Query failed to execute' : $e->getMessage();
        $code = 400;
        $action = '';

        return compact('code', 'message', 'action');
    }

    public static function clientException(Throwable $e): array
    {
        $message = (config('app.env') == 'production') ? 'Failed fetching http request' : $exception->getMessage();
        $code = $exceptionCode;
        $action = '';

        return compact('code', 'message', 'action');
    }
}
