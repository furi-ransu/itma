<?php

namespace App\Actions\WebUi\Otp\Verify;

use App\Models\User;
use App\Models\UserOtp;
use Illuminate\Http\Request;

class VerifyOtp
{
    /**
     * Handle the datatable request.
     */
    public static function handle(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $userOtp = UserOtp::latest()->where('redeem_at', null)
            ->where('code', $request->code)->where('user_id', $user->id)
            ->latest()->first();

        if (is_null($userOtp)) {
            throw new \Exception('Invalid code');
        }

        $userOtp->update(['redeem_at' => now()]);
        session(['otp_verified' => true]);
    }
}
