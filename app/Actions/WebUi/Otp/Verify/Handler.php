<?php

namespace App\Actions\WebUi\Otp\Verify;

use Illuminate\Http\Request;

class Handler
{
    /**
     * Handle the datatable request.
     */
    public function handle(Request $request)
    {
        ValidateRequest::handle($request);
        VerifyOtp::handle($request);
    }
}
