<?php

namespace App\Actions\Api\Datatable\Project;

use App\Models\Project as ProjectModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = ProjectModel::query()->with([
            'lead', 'invoice', 'branch_department.branch', 'client', 'branch_department.department',
            'emp.user', 'project_status', 'project_type', 'service_category',
            'service_item',
        ])->select('projects.*');

        return DataTables::eloquent($model)->toJson();
    }
}
