<?php

namespace App\Actions\Api\Datatable\ServiceCategory;

use App\Models\ServiceCategory as ServiceCategoryModel;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    /**
     * Handle the datatable request.
     */
    public function handle(Request $request)
    {
        $model = ServiceCategoryModel::query();

        return DataTables::eloquent($model)->toJson();
    }
}
