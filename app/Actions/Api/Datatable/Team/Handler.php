<?php

namespace App\Actions\Api\Datatable\Team;

use App\Models\Team as TeamModel;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    /**
     * Handle the datatable request.
     */
    public function handle(Request $request)
    {
        $model = TeamModel::query()->with([
            'branch_department.branch',
            'branch_department.department',
        ])->select('teams.*');

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.team.index.parts.datatable.column_action', [
                    'route_edit' => route('cms.teams.edit', $row->id),
                    'route_delete' => route('api.cms.team.delete', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
