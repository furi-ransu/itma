<?php

namespace App\Actions\Api\Datatable\Client;

use App\Models\Client as ClientModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = ClientModel::query()->select('clients.*');

        return DataTables::eloquent($model)->toJson();
    }
}
