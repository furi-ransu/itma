<?php

namespace App\Actions\Api\Datatable\Invoice;

use App\Models\Invoice as InvoiceModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = InvoiceModel::query()->select('invoices.*');

        return DataTables::eloquent($model)->toJson();
    }
}
