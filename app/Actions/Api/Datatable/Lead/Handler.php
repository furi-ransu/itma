<?php

namespace App\Actions\Api\Datatable\Lead;

use App\Models\Lead as LeadModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = LeadModel::query()->select('leads.*');

        return DataTables::eloquent($model)->toJson();
    }
}
