<?php

namespace App\Actions\Api\Datatable\BranchDepartment;

use App\Models\BranchDepartment as BranchDepartmentModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = BranchDepartmentModel::query()->with([
            'branch.city.state.country',
            'department',
        ])->select('branch_departments.*');

        return DataTables::eloquent($model)->toJson();
    }
}
