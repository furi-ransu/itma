<?php

namespace App\Actions\Api\Datatable\ServiceGroup;

use App\Models\ServiceGroup as ServiceGroupModel;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    /**
     * Handle the datatable request.
     */
    public function handle(Request $request)
    {
        $model = ServiceGroupModel::query();

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.service-group.index.parts.datatable.column_action', [
                    'route_edit' => route('cms.service-groups.edit', $row->id),
                    'route_delete' => route('api.cms.service-group.delete', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
