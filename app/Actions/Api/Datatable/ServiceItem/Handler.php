<?php

namespace App\Actions\Api\Datatable\ServiceItem;

use App\Models\ServiceItem as ServiceItemModel;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    /**
     * Handle the datatable request.
     */
    public function handle(Request $request)
    {
        $model = ServiceItemModel::query()->with(['service_group']);

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.service-item.index.parts.datatable.column_action', [
                    'route_edit' => route('cms.service-items.edit', $row->id),
                    'route_delete' => route('api.cms.service-item.delete', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
