<?php

namespace App\Actions\Api\Datatable\Branch;

use App\Models\Branch as BranchModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = BranchModel::query()->with(['city.state.country'])->select('branches.*');

        return DataTables::eloquent($model)->toJson();
    }
}
