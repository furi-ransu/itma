<?php

namespace App\Actions\Api\Datatable\City;

use App\Models\City as CityModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = CityModel::query()->limit(1000)->with(['state.country'])->select('cities.*');

        return DataTables::eloquent($model)->toJson();
    }
}
