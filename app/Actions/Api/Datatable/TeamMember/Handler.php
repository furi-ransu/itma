<?php

namespace App\Actions\Api\Datatable\TeamMember;

use App\Models\TeamMember as TeamMemberModel;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    /**
     * Handle the datatable request.
     */
    public function handle(Request $request)
    {
        $model = TeamMemberModel::query()->with([
            'team.branch_department.branch',
            'team.branch_department.department',
            'emp.user',
        ])->select('team_members.*');

        return DataTables::eloquent($model)->toJson();
    }
}
