<?php

namespace App\Actions\Api\Datatable\Emp;

use App\Models\Emp as EmpModel;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = EmpModel::query()->with([
            'branch_department.branch.city.state.country',
            'branch_department.department',
            'user',
        ])->select('emps.*');

        return DataTables::eloquent($model)->toJson();
    }
}
