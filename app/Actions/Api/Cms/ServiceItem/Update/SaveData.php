<?php

namespace App\Actions\Api\Cms\ServiceItem\Update;

use App\Models\ServiceItem;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        ServiceItem::where('id', $request->id)->update($request->only(['name', 'service_group_id']));
    }
}
