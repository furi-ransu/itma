<?php

namespace App\Actions\Api\Cms\ServiceItem\Update;

use App\Models\ServiceItem;
use Illuminate\Validation\Rule;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'string',
                'max:100',
                Rule::unique('service_items')->ignore($request->id),
            ],
            'service_group_id' => 'required|exists:service_groups,id',
        ]);

        self::preventDuplicate($request);
    }

    public static function preventDuplicate($request)
    {
        $serviceItem = ServiceItem::where('name', $request->name)
            ->where('service_group_id', $request->service_group_id)
            ->where('id', '<>', $request->id)
            ->first();
        if ($serviceItem) {
            throw new \Exception('Service item already exists');
        }
    }
}
