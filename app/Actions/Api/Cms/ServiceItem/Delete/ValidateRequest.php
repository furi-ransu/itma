<?php

namespace App\Actions\Api\Cms\ServiceItem\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:service_items,id',
        ]);
    }
}
