<?php

namespace App\Actions\Api\Cms\ServiceItem\Delete;

class Handler
{
    public function handle(\Illuminate\Http\Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $request->merge([
            'id' => $id,
        ]);
        ValidateRequest::handle($request);
        \App\Models\ServiceItem::destroy($id);

        return response()->apicms(200, 'success', [], 'Service item deleted successfully');
    }
}
