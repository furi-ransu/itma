<?php

namespace App\Actions\Api\Cms\ServiceItem\Store;

use App\Models\ServiceItem;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        ServiceItem::create($request->only(['name', 'service_group_id']));
    }
}
