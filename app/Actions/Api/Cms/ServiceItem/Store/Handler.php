<?php

namespace App\Actions\Api\Cms\ServiceItem\Store;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        ValidateRequest::handle($request);
        SaveData::handle($request);

        return response()->apicms(200, 'success', [], 'Service item created successfully');
    }
}
