<?php

namespace App\Actions\Api\Cms\ServiceItem\Store;

use App\Models\ServiceItem;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'name' => 'required|unique:service_items,name',
            'service_group_id' => 'required|exists:service_groups,id',
        ]);

        self::preventDuplicate($request);
    }

    public static function preventDuplicate($request)
    {
        $serviceItem = ServiceItem::where('name', $request->name)
            ->where('service_group_id', $request->service_group_id)
            ->first();
        if ($serviceItem) {
            throw new \Exception('Service item already exists');
        }
    }
}
