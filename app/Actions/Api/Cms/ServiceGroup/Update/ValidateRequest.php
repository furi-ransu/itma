<?php

namespace App\Actions\Api\Cms\ServiceGroup\Update;

use Illuminate\Validation\Rule;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|numeric|exists:service_groups,id',
            'name' => [
                'required',
                'string',
                'max:100',
                Rule::unique('service_groups')->ignore($request->id),
            ],
        ]);
    }
}
