<?php

namespace App\Actions\Api\Cms\ServiceGroup\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:service_groups,id',
        ]);
    }
}
