<?php

namespace App\Actions\Api\Cms\ServiceGroup\Delete;

class Handler
{
    public function handle(\Illuminate\Http\Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $request->merge([
            'id' => $id,
        ]);
        ValidateRequest::handle($request);
        \App\Models\ServiceGroup::destroy($id);

        return response()->apicms(200, 'success', [], 'Service group deleted successfully');
    }
}
