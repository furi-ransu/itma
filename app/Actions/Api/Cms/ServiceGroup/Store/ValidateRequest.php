<?php

namespace App\Actions\Api\Cms\ServiceGroup\Store;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:100|unique:service_groups,name',
        ]);
    }
}
