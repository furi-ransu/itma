<?php

namespace App\Actions\Api\Cms\Team\Store;

use App\Models\Team;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Team::create($request->only(['name', 'branch_department_id']));
    }
}
