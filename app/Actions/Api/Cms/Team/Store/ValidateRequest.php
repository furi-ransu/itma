<?php

namespace App\Actions\Api\Cms\Team\Store;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:teams',
            'branch_department_id' => 'required|exists:branch_departments,id',
        ]);

        self::preventDuplicate($request);
    }

    private static function preventDuplicate(\Illuminate\Http\Request $request)
    {
        $team = \App\Models\Team::where('name', $request->name)->where('branch_department_id', $request->branch_department_id)->first();
        if ($team) {
            throw new \Exception('Team with same name already exists.');
        }
    }
}
