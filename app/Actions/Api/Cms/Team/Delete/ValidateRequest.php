<?php

namespace App\Actions\Api\Cms\Team\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:teams,id',
        ]);
    }
}
