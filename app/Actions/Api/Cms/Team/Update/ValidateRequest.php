<?php

namespace App\Actions\Api\Cms\Team\Update;

use Illuminate\Validation\Rule;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:teams,id',
            'name' => [
                'required',
                'string',
                'max:255',
                Rule::unique('teams')->ignore($request->id),
            ],
        ]);
    }
}
