<?php

namespace App\Actions\Api\Cms\Team\Update;

class Handler
{
    public function handle(\Illuminate\Http\Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $request->merge([
            'id' => $id,
        ]);
        ValidateRequest::handle($request);
        SaveData::handle($request);

        return response()->apicms(200, 'success', [], 'Team updated successfully');
    }
}
