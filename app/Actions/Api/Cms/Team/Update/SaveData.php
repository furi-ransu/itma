<?php

namespace App\Actions\Api\Cms\Team\Update;

use App\Models\Team;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Team::where('id', $request->id)->update($request->only(['name']));
    }
}
