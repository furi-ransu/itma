<?php

namespace App\Actions\Api\Cms\Project\Store;

class GetDataBranchDepartmentUser
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $user = \App\Models\User::find($request->user_id);
        $emp = \App\Models\Emp::where('user_id', $user->id)->first();

        $request->merge([
            'user' => $user,
            'emp_id' => $emp->id,
        ]);
    }
}
