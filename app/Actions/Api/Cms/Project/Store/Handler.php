<?php

namespace App\Actions\Api\Cms\Project\Store;

use DB;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        ValidateRequest::handle($request);

        DB::beginTransaction();
        try {
            GetDataBranchDepartmentUser::handle($request);
            SaveData::handle($request);
            SendEmail::handle($request);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }

        return response()->apicms(200, 'success', [], 'Project plan created successfully');
    }
}
