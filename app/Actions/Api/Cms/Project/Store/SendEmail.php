<?php

namespace App\Actions\Api\Cms\Project\Store;

use App\Jobs\SendEmailToManager;
use App\Models\User;

class SendEmail
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $departmentManagers = User::role('department manager')->get();
        $seniorManagers = User::role('senior manager')->get();

        if ($departmentManagers->count() > 0) {
            foreach ($departmentManagers as $key => $value) {
                dispatch(new SendEmailToManager($value->email, $request->number));
            }
        }

        if ($seniorManagers->count() > 0) {
            foreach ($seniorManagers as $key => $value) {
                dispatch(new SendEmailToManager($value->email, $request->number));
            }
        }
    }
}
