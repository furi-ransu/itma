<?php

namespace App\Actions\Api\Cms\Project\Store;

use App\Enums\ActiveStatusEnum;
use App\Enums\RoleEnum;
use App\Models\Project;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'number' => 'required|string|max:255|unique:projects,number',
            'lead_id' => 'required|exists:leads,id',
            'client_id' => 'required|exists:clients,id',
            'invoice_id' => 'required|exists:invoices,id',
            'service_item_id' => 'required|exists:service_items,id',
            'service_category_id' => 'required|exists:service_categories,id',
            'user_id' => 'required|exists:users,id',
        ]);

        self::preventDuplicateLeadId($request);
        self::preventDuplicateInvoiceId($request);
        self::validateUserIdIConsultant($request);
    }

    public static function validateUserIdIConsultant($request)
    {
        $user = \App\Models\User::find($request->user_id);
        $emp = \App\Models\Emp::with(['branch_department.department'])->where('user_id', $user->id)->first();
        $roleUser = $user->getRoleNames()->first();

        if ($emp->branch_department->department->name !== 'consultant') {
            throw new \Exception('Only Consultants can create project plans');
        }

        if (ActiveStatusEnum::YES()->getValue() != $user->active_status) {
            throw new \Exception('Consultant is not active');
        }
    }

    public static function preventDuplicateLeadId($request)
    {
        $project = Project::where('lead_id', $request->lead_id)->first();

        if ($project) {
            throw new \Exception('Lead already has a project');
        }
    }

    public static function preventDuplicateInvoiceId($request)
    {
        $project = Project::where('invoice_id', $request->invoice_id)->first();

        if ($project) {
            throw new \Exception('Invoice already has a project');
        }
    }
}
