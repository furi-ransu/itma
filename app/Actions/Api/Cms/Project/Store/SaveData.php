<?php

namespace App\Actions\Api\Cms\Project\Store;

use App\Models\Project;
use App\Models\ProjectStatus;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $data = $request->only([
            'number',
            'lead_id',
            'client_id',
            'invoice_id',
            'branch_department_id',
            'service_item_id',
            'service_category_id',
            'project_type_id',
            'project_status_id',
        ]);
        $data['consultant_id'] = $request->emp_id;
        $data['project_status_id'] = ProjectStatus::first()->id;
        Project::create($data);
    }
}
