<?php

namespace App\Actions\Api\Cms\TeamMember\Update;

use App\Models\Employee;
use App\Models\Team;
use App\Models\TeamMember;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:team_members,id',
            'team_id' => 'required|exists:teams,id',
            'employee_id' => 'required|exists:employees,id',
        ]);

        self::preventDuplicate($request);
        self::allowSpesificRoles($request);
        self::checkDepartment($request);
    }

    private static function checkDepartment(\Illuminate\Http\Request $request)
    {
        $employee = Employee::with(['department'])->find($request->employee_id);
        $team = Team::with(['department'])->find($request->team_id);
        if ($employee->department->id != $team->department_id) {
            throw new \Exception('Department not match');
        }
    }

    private static function allowSpesificRoles(\Illuminate\Http\Request $request)
    {
        $employee = Employee::with(['user'])->find($request->employee_id);
        $user = $employee->user;
        $roleName = $user->getRoleNames()->first();
        $allowRoles = config('itma.team_member.roles');
        if (! in_array($roleName, $allowRoles)) {
            throw new \Exception('User role not allowed');
        }
    }

    private static function preventDuplicate(\Illuminate\Http\Request $request)
    {
        $teamMemberExist = TeamMember::where('employee_id', $request->employee_id)
            ->where('team_id', $request->team_id)
            ->where('id', '!=', $request->id)
            ->first();

        if ($teamMemberExist) {
            throw new \Exception('Team member already exists');
        }
    }
}
