<?php

namespace App\Actions\Api\Cms\TeamMember\Update;

use App\Models\TeamMember;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        TeamMember::where('id', $request->id)->update($request->only(['team_id', 'employee_id']));
    }
}
