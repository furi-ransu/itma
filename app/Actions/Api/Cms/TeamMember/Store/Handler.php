<?php

namespace App\Actions\Api\Cms\TeamMember\Store;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        ValidateRequest::handle($request);
        SaveData::handle($request);

        return response()->apicms(200, 'success', [], 'Team member created successfully');
    }
}
