<?php

namespace App\Actions\Api\Cms\TeamMember\Store;

use App\Models\TeamMember;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'team_id' => 'required|exists:teams,id',
            'emp_id' => 'required|exists:emps,id',
        ]);

        self::preventDuplicate($request);
    }

    private static function preventDuplicate(\Illuminate\Http\Request $request)
    {
        $teamMember = TeamMember::where('team_id', $request->team_id)->where('emp_id', $request->emp_id)->first();
        if ($teamMember) {
            throw new \Exception('Team member already exists');
        }
    }
}
