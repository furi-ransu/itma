<?php

namespace App\Actions\Api\Cms\TeamMember\Store;

use App\Models\TeamMember;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        TeamMember::create($request->only([
            'team_id',
            'emp_id',
        ]));
    }
}
