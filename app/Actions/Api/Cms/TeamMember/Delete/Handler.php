<?php

namespace App\Actions\Api\Cms\TeamMember\Delete;

class Handler
{
    public function handle(\Illuminate\Http\Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $request->merge([
            'id' => $id,
        ]);
        ValidateRequest::handle($request);
        \App\Models\TeamMember::destroy($id);

        return response()->apicms(200, 'success', [], 'Team member deleted successfully');
    }
}
