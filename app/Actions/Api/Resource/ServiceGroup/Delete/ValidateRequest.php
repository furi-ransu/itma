<?php

namespace App\Actions\Api\Resource\ServiceGroup\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:service_groups,id',
        ]);
    }
}
