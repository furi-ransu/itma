<?php

namespace App\Actions\Api\Resource\ServiceGroup\Lists;

use App\Models\ServiceGroup;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = ServiceGroup::query()->select('service_groups.*');

        return DataTables::eloquent($model)->toJson();
    }
}
