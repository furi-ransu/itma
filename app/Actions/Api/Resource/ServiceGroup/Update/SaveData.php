<?php

namespace App\Actions\Api\Resource\ServiceGroup\Update;

use App\Models\ServiceGroup;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        ServiceGroup::where('id', $request->id)->update($request->only(['name']));
    }
}
