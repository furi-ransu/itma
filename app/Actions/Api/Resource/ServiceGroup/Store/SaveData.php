<?php

namespace App\Actions\Api\Resource\ServiceGroup\Store;

use App\Models\ServiceGroup;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        ServiceGroup::create($request->only(['name']));
    }
}
