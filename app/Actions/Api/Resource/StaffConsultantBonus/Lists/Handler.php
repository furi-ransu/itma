<?php

namespace App\Actions\Api\Resource\StaffConsultantBonus\Lists;

use App\Models\PicCb;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = PicCb::query()->with(['currency', 'emp.user', 'md_employment_status'])->select('pic_cbs.*');

        return DataTables::eloquent($model)->toJson();
    }
}
