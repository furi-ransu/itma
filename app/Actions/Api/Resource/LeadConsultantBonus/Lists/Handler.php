<?php

namespace App\Actions\Api\Resource\LeadConsultantBonus\Lists;

use App\Models\LeadCb;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = LeadCb::query()->with(['currency', 'emp.user'])->select('lead_cbs.*');

        return DataTables::eloquent($model)->toJson();
    }
}
