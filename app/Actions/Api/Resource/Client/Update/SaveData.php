<?php

namespace App\Actions\Api\Resource\Client\Update;

use App\Models\Client;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Client::where('id', $request->id)->update($request->only([
            'active_status',
            'name',
            'type',
            'email',
            'phone_number',
            'address',
            'kyc_url_document',
            'city_id',
            'md_kyc_status_id',
        ]));
    }
}
