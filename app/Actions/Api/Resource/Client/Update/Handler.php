<?php

namespace App\Actions\Api\Resource\Client\Update;

class Handler
{
    public function handle(\Illuminate\Http\Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $request->merge(['id' => $id]);
        ValidateRequest::handle($request);
        SaveData::handle($request);

        return response()->api(200, 'success', [], 'Client updated successfully');
    }
}
