<?php

namespace App\Actions\Api\Resource\Client\Update;

use Illuminate\Validation\Rule;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:clients,id',
            'name' => 'required|string|max:100',
            'type' => 'required|in:individual,corporate',
            'address' => 'required|string|max:255',
            'phone_number' => [
                'required',
                'string',
                'max:20',
                Rule::unique('clients')->ignore($request->id),
            ],
            'email' => [
                'required',
                'email',
                'max:100',
                Rule::unique('clients')->ignore($request->id),
            ],
            'kyc_url_document' => 'required|url',
            'md_kyc_status_id' => 'required|exists:md_kyc_statuses,id',
            'city_id' => 'required|exists:cities,id',
            'active_status' => 'required|in:no,yes',
        ]);
    }
}
