<?php

namespace App\Actions\Api\Resource\Client\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:clients,id',
        ]);
    }
}
