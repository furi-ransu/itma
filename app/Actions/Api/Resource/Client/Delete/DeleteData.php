<?php

namespace App\Actions\Api\Resource\Client\Delete;

use App\Models\Client;

class DeleteData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        // TODO: jika ada id client di table projects, gagalkan
        Client::where('id', $request->id)->delete();
    }
}
