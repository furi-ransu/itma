<?php

namespace App\Actions\Api\Resource\Client\Store;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:100',
            'type' => 'required|in:individual,corporate',
            'address' => 'required|string|max:255',
            'phone_number' => 'required|string|max:20|unique:clients',
            'email' => 'required|email|max:100|unique:clients',
            'kyc_url_document' => 'required|url',
            'md_kyc_status_id' => 'required|exists:md_kyc_statuses,id',
            'city_id' => 'required|exists:cities,id',
        ]);
    }
}
