<?php

namespace App\Actions\Api\Resource\Client\Store;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        ValidateRequest::handle($request);
        SaveData::handle($request);

        return response()->api(200, 'success', [], 'Client created successfully');
    }
}
