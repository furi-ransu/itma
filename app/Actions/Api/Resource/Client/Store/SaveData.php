<?php

namespace App\Actions\Api\Resource\Client\Store;

use App\Models\Client;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Client::create($request->only([
            'name',
            'type',
            'email',
            'phone_number',
            'address',
            'kyc_url_document',
            'city_id',
            'md_kyc_status_id',
        ]));
    }
}
