<?php

namespace App\Actions\Api\Resource\Client\Lists;

use App\Models\Client;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = Client::query()->with(['city.state.country', 'md_kyc_status'])->select('clients.*');

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.client.lists.includes.column-action.client-datatable', [
                    'route_edit' => route('cms.clients.edit', $row->id),
                    'route_delete' => route('api.resource.client.delete', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
