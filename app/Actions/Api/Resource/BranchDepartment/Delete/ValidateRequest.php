<?php

namespace App\Actions\Api\Resource\BranchDepartment\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:branch_departments,id',
        ]);
    }
}
