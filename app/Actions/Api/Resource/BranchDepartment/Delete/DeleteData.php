<?php

namespace App\Actions\Api\Resource\BranchDepartment\Delete;

use App\Models\BranchDepartment;

class DeleteData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        BranchDepartment::where('id', $request->id)->delete();
    }
}
