<?php

namespace App\Actions\Api\Resource\BranchDepartment\Lists;

use App\Models\BranchDepartment;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = BranchDepartment::query()->with([
            'branch.city', 'branch.city.state',
            'branch.city.state.country', 'department',
        ])
            ->select('branch_departments.*');

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.branch-department.lists.includes.column-action.branch-department-datatable', [
                    'route_edit' => route('cms.branch-departments.edit', $row->id),
                    'route_delete' => route('api.resource.branch-department.delete', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
