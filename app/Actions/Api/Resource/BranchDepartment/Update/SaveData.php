<?php

namespace App\Actions\Api\Resource\BranchDepartment\Update;

use App\Models\BranchDepartment;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        BranchDepartment::where('id', $request->id)->update($request->only(['branch_id', 'department_id']));
    }
}
