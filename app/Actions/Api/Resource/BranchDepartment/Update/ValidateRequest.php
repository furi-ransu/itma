<?php

namespace App\Actions\Api\Resource\BranchDepartment\Update;

use App\Models\BranchDepartment;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:branch_departments,id',
            'branch_id' => 'required|exists:branches,id',
            'department_id' => 'required|exists:departments,id',
        ]);

        self::preventDuplicate($request);
    }

    private static function preventDuplicate($request)
    {
        $branchDepartment = BranchDepartment::where('branch_id', $request->branch_id)
            ->where('department_id', $request->department_id)
            ->where('id', '!=', $request->id)
            ->first();

        if ($branchDepartment) {
            throw new \Exception('Branch department already exists');
        }
    }
}
