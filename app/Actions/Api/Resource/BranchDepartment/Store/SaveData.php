<?php

namespace App\Actions\Api\Resource\BranchDepartment\Store;

use App\Models\BranchDepartment;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        BranchDepartment::create($request->only(['branch_id', 'department_id']));
    }
}
