<?php

namespace App\Actions\Api\Resource\Setting\Store;

use App\Models\Setting;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Setting::create($request->only(['key', 'value']));
    }
}
