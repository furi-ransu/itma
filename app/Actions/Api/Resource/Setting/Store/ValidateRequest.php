<?php

namespace App\Actions\Api\Resource\Setting\Store;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'key' => 'required|string|max:255|unique:settings,key',
            'value' => 'required|string|max:255',
        ]);
    }
}
