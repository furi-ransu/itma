<?php

namespace App\Actions\Api\Resource\Setting\Update;

use Illuminate\Validation\Rule;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:settings,id',
            'value' => 'required|string|max:255',
            'key' => [
                'required',
                'string',
                'max:255',
                Rule::unique('settings')->ignore($request->id),
            ],
        ]);
    }
}
