<?php

namespace App\Actions\Api\Resource\Setting\Update;

use App\Models\Setting;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Setting::where('id', $request->id)->update($request->only(['key', 'value']));
    }
}
