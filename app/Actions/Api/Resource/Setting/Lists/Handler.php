<?php

namespace App\Actions\Api\Resource\Setting\Lists;

use App\Models\Setting;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = Setting::query()->select('settings.*');

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.setting.lists.includes.column-action.setting-datatable', [
                    'route_edit' => route('cms.settings.edit', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
