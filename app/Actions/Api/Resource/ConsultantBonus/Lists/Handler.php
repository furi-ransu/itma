<?php

namespace App\Actions\Api\Resource\ConsultantBonus\Lists;

use App\Models\ConsultantBonus;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = ConsultantBonus::query()->select('consultant_bonuses.*');

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.consultant-bonus.lists.includes.column-action.consultant-bonus-datatable', [
                    'route_show' => route('cms.branches.show', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
