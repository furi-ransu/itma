<?php

namespace App\Actions\Api\Resource\Invoice\Lists;

use App\Models\Invoice;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = Invoice::query()->select('invoices.*');

        return DataTables::eloquent($model)->toJson();
    }
}
