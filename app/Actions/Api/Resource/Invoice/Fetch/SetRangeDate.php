<?php

namespace App\Actions\Api\Resource\Invoice\Fetch;

class SetRangeDate
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $startDateOfYear = \Carbon\Carbon::create($request->year, 1, 1)->toDateString();
        $lastDateOfYear = \Carbon\Carbon::create($request->year, 12, 31)->toDateString();

        $request->merge([
            'start_date' => \Carbon\Carbon::parse($startDateOfYear)->format('d/m/Y'),
            'end_date' => \Carbon\Carbon::parse($lastDateOfYear)->format('d/m/Y'),
        ]);
    }
}
