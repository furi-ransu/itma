<?php

namespace App\Actions\Api\Resource\Invoice\Fetch;

class HitAccurate
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $baseUrl = config('itma.accurate.public_url');
        $inputString = 'customerNo='.$request->customer_number.'&fromDate='.$request->start_date.'&toDate='.$request->end_date;
        parse_str($inputString, $params);
        $queryString = http_build_query($params);
        $url = $baseUrl.'/accurate/api/sales-invoice/detail-invoice.do?'.$queryString;
        $response = hit('GET', $url, [
            'Authorization: Bearer '.$request->oauth_access_token,
            'X-Session-ID: '.$request->x_session_id,
        ], []);

        $request->merge([
            'body_response_fetch_sales_invoice' => $response,
        ]);
    }
}
