<?php

namespace App\Actions\Api\Resource\Invoice\Fetch;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $invoices = $request->data_invoice;
        foreach ($invoices as $key => $value) {
            $invoiceExist = \App\Models\Invoice::where('number', $value['number'])->first();
            $data = [
                'po_number' => $value['poNumber'],
                'tax_number' => $value['taxNumber'],
                'due_date' => \DateTime::createFromFormat('d/m/Y', $value['dueDate'])->format('Y-m-d'),
                'transaction_date' => \DateTime::createFromFormat('d/m/Y', $value['transDate'])->format('Y-m-d'),
                'customer_name' => $request->customer_name,
                'customer_number' => $request->customer_number,
                'currency_iso_code' => $request->currency_code,
                'status' => $value['status'],
                'nominal' => $value['primeReceipt'],
            ];
            if ($invoiceExist) {
                $invoiceExist->update($data);
            } else {
                $data['number'] = $value['number'];
                \App\Models\Invoice::create($data);
            }
        }
    }
}
