<?php

namespace App\Actions\Api\Resource\Invoice\Fetch;

class GetOauthTokenSession
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $oauthAccessToken = \App\Models\Setting::where('key', 'accurate_access_token')->first()->value;
        $xSessionId = \App\Models\Setting::where('key', 'accurate_session_id')->first()->value;
        $request->merge([
            'oauth_access_token' => $oauthAccessToken,
            'x_session_id' => $xSessionId,
        ]);
    }
}
