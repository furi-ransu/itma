<?php

namespace App\Actions\Api\Resource\Invoice\Fetch;

class HitAccurateGetCustomer
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $invoice = $request->data_invoice_first;
        $invoiceId = $invoice['id'];
        $baseUrl = config('itma.accurate.public_url');
        $inputString = 'id='.$invoiceId;
        parse_str($inputString, $params);
        $queryString = http_build_query($params);
        $url = $baseUrl.'/accurate/api/sales-invoice/detail.do?'.$queryString;
        $response = hit('GET', $url, [
            'Authorization: Bearer '.$request->oauth_access_token,
            'X-Session-ID: '.$request->x_session_id,
        ], []);

        $bodyResponseArray = json_decode($response, true);

        if (! array_key_exists('s', $bodyResponseArray)) {
            throw new \Exception('Invalid sales invoice response not found s key');
        }

        if (! array_key_exists('d', $bodyResponseArray)) {
            throw new \Exception('Invalid sales invoice response not found d key');
        }

        if ($bodyResponseArray['s'] == false) {
            throw new \Exception('Invalid sales invoice response');
        }

        $data = $bodyResponseArray['d'];
        $request->merge([
            'customer_name' => $data['customer']['wpName'],
            'customer_number' => $data['customer']['customerNo'],
            'currency_code' => $data['currency']['code'],
        ]);
    }
}
