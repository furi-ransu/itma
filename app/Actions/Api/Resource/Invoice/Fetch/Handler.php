<?php

namespace App\Actions\Api\Resource\Invoice\Fetch;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'customer_number' => 'required',
            'year' => 'required|date_format:Y',
        ]);
        SetRangeDate::handle($request);
        GetOauthTokenSession::handle($request);
        HitAccurate::handle($request);
        ValidateResponseSalesInvoice::handle($request);
        HitAccurateGetCustomer::handle($request);
        SaveData::handle($request);

        return response()->api(200, 'success', [], 'Invoice fetch successfully');
    }
}
