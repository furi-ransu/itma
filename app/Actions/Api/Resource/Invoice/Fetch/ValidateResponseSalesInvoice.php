<?php

namespace App\Actions\Api\Resource\Invoice\Fetch;

class ValidateResponseSalesInvoice
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $salesInvoiceResponse = $request->body_response_fetch_sales_invoice;
        $salesInvoiceResponseArray = json_decode($salesInvoiceResponse, true);

        if (! array_key_exists('s', $salesInvoiceResponseArray)) {
            throw new \Exception('Invalid sales invoice response not found s key');
        }

        if (! array_key_exists('d', $salesInvoiceResponseArray)) {
            throw new \Exception('Invalid sales invoice response not found d key');
        }

        if ($salesInvoiceResponseArray['s'] == false) {
            throw new \Exception('Invalid sales invoice response');
        }

        $data = $salesInvoiceResponseArray['d'];

        if (count($data) < 1) {
            throw new \Exception('Invoice sales number not found');
        }

        $request->merge([
            'data_invoice' => $data,
            'data_invoice_first' => $data[0],
        ]);
    }
}
