<?php

namespace App\Actions\Api\Resource\Lead\Fetch;

use App\Models\Lead;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $bodyResponseArray = json_decode($request->body_response_fetch_odoo_partner, true);

        if (! is_array($bodyResponseArray)) {
            throw new \Exception('Invalid response from itma odoo server');
        }

        $leadExist = Lead::where('odoo_lead_id', $request->id)->first();
        if ($leadExist) {
            $leadExist->where('id', $leadExist->id)
                ->update([
                    'name' => $bodyResponseArray['lead']['name'],
                    'date' => \Carbon\Carbon::parse($bodyResponseArray['lead']['date_open']),
                    'description' => ($bodyResponseArray['lead']['description'] == false) ? '' : $bodyResponseArray['lead']['description'],
                    'is_won' => ($bodyResponseArray['lead']['won_status'] == 'won') ? yes : no,
                    'odoo_lead_id' => $bodyResponseArray['lead']['id'],
                    'odoo_partner_id' => $bodyResponseArray['partner']['id'],
                    'odoo_partner_name' => ($bodyResponseArray['partner']['display_name'] == false) ? '' : $bodyResponseArray['partner']['display_name'],
                    'odoo_partner_email' => ($bodyResponseArray['partner']['email_normalized'] == false) ? '' : $bodyResponseArray['partner']['email_normalized'],
                    'odoo_partner_phone_number' => ($bodyResponseArray['partner']['phone_sanitized'] == false) ? '' : $bodyResponseArray['partner']['phone_sanitized'],
                    'detail' => json_encode($bodyResponseArray),
                ]);
        } else {
            Lead::create([
                'name' => $bodyResponseArray['data']['lead']['name'],
                'date' => \Carbon\Carbon::parse($bodyResponseArray['data']['lead']['date_open']),
                'description' => ($bodyResponseArray['data']['lead']['description'] == false) ? '' : $bodyResponseArray['data']['lead']['description'],
                'is_won' => ($bodyResponseArray['data']['lead']['won_status'] == 'won') ? 'yes' : 'no',
                'odoo_lead_id' => $bodyResponseArray['data']['lead']['id'],
                'odoo_partner_id' => $bodyResponseArray['data']['partner']['id'],
                'odoo_partner_name' => ($bodyResponseArray['data']['partner']['display_name'] == false) ? '' : $bodyResponseArray['data']['partner']['display_name'],
                'odoo_partner_email' => ($bodyResponseArray['data']['partner']['email_normalized'] == false) ? '' : $bodyResponseArray['data']['partner']['email_normalized'],
                'odoo_partner_phone_number' => ($bodyResponseArray['data']['partner']['phone_sanitized'] == false) ? '' : $bodyResponseArray['data']['partner']['phone_sanitized'],
                'detail' => json_encode($bodyResponseArray['data']),
            ]);
        }
    }
}
