<?php

namespace App\Actions\Api\Resource\Lead\Fetch;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'id' => 'required|numeric',
        ]);
        Hit::handle($request);
        SaveData::handle($request);

        return response()->api(200, 'success', [], 'Lead fetch successfully');
    }
}
