<?php

namespace App\Actions\Api\Resource\Lead\Fetch;

class Hit
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $baseUrl = config('itma.itma_odoo.url');
        $url = $baseUrl.'/api/odoo/lead/'.$request->id;
        $response = hit('GET', $url, [], '');

        if (isJson($response) == false) {
            throw new \Exception('Invalid response from itma odoo server');
        }

        $request->merge([
            'body_response_fetch_odoo_partner' => $response,
        ]);
    }
}
