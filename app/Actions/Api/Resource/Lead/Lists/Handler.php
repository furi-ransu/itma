<?php

namespace App\Actions\Api\Resource\Lead\Lists;

use App\Models\Lead;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = Lead::query()->select('leads.*');

        return DataTables::eloquent($model)->toJson();
    }
}
