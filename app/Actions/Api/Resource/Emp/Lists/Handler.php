<?php

namespace App\Actions\Api\Resource\Emp\Lists;

use App\Models\Emp;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = Emp::query()->with([
            'user',
            'branch_department.branch',
            'branch_department.department',
            'md_employment_status',
        ])->select('emps.*');

        return DataTables::eloquent($model)
            ->addColumn('role_name', function ($row) {
                return $row->user->getRoleNames()->first();
            })
            ->addColumn('actions', function ($row) {
                return view('cms.emp.lists.includes.column-action.emp-datatable', [
                    'route_edit' => route('cms.emps.edit', $row->id),
                    'route_delete' => route('api.resource.emp.delete', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
