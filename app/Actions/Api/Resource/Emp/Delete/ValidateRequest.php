<?php

namespace App\Actions\Api\Resource\Emp\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:emps,id',
        ]);
    }
}
