<?php

namespace App\Actions\Api\Resource\Emp\Delete;

use App\Models\Emp;
use App\Models\User;

class DeleteData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $emp = Emp::where('id', $request->id)->first();
        $userId = $emp->user_id;
        Emp::where('id', $request->id)->delete();
        User::where('id', $userId)->delete();
    }
}
