<?php

namespace App\Actions\Api\Resource\Emp\Store;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'number' => 'required|string|max:255|unique:emps',
            'branch_department_id' => 'required|exists:branch_departments,id',
            'md_employment_status_id' => 'required|exists:md_employment_statuses,id',
            'role_id' => 'required|exists:roles,id',
        ]);
    }
}
