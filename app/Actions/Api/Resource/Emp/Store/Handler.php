<?php

namespace App\Actions\Api\Resource\Emp\Store;

use App\Jobs\SendEmailPasswordToEmployee;
use DB;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            ValidateRequest::handle($request);
            SaveData::handle($request);
            dispatch(new SendEmailPasswordToEmployee($request->name, $request->email, $request->random_password));
            DB::commit();

            return response()->api(200, 'success', [], 'Employee created successfully');
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->api(400, 'fail', [], $e->getMessage());
        }
    }
}
