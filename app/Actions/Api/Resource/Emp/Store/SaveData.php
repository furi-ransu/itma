<?php

namespace App\Actions\Api\Resource\Emp\Store;

use App\Models\Emp;
use App\Models\Role;
use App\Models\User;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        self::createUser($request);
        self::createEmp($request);
    }

    public static function createUser($request)
    {
        $request->merge([
            'raw_password' => 'pokemon',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->raw_password),
        ]);

        $request->merge([
            'user_id' => $user->id,
        ]);
        $role = Role::where('id', $request->role_id)->first();
        $user->assignRole($role->name);
    }

    public static function createEmp($request)
    {
        Emp::create($request->only([
            'number',
            'user_id',
            'md_employment_status_id',
            'branch_department_id',
        ]));
    }
}
