<?php

namespace App\Actions\Api\Resource\Emp\Update;

class Handler
{
    public function handle(\Illuminate\Http\Request $request, $id): \Illuminate\Http\JsonResponse
    {
        try {
            $request->merge([
                'id' => $id,
                'emp_id' => $id,
            ]);
            ValidateRequest::handle($request);
            SaveData::handle($request);

            return response()->api(200, 'success', [], 'Employee updated successfully');
        } catch (\Exception $e) {
            return response()->api(400, 'fail', [], $e->getMessage());
        }
    }
}
