<?php

namespace App\Actions\Api\Resource\Emp\Update;

use App\Models\Emp;
use App\Models\Role;
use App\Models\User;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        self::updateUser($request);
        self::updateEmp($request);
    }

    public static function updateUser($request)
    {
        User::where('id', $request->user_id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'active_status' => $request->active_status,
        ]);
        $user = User::find($request->user_id);
        $role = Role::where('id', $request->role_id)->first();
        $user->syncRoles([$role->name]);
    }

    public static function updateEmp($request)
    {
        Emp::where('id', $request->id)->update($request->only([
            'number',
            'md_employment_status_id',
            'branch_department_id',
        ]));
    }
}
