<?php

namespace App\Actions\Api\Resource\Emp\Update;

use App\Models\Emp;
use Illuminate\Validation\Rule;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:emps,id',
        ]);
        $emp = Emp::find($request->id);
        $request->merge([
            'user_id' => $emp->user_id,
        ]);
        $request->validate([
            'active_status' => 'required|in:no,yes',
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore($request->user_id),
            ],
            'number' => [
                'required',
                'string',
                'max:255',
                Rule::unique('emps')->ignore($request->id),
            ],
            'branch_department_id' => 'nullable|exists:branch_departments,id',
            'md_employment_status_id' => 'required|exists:md_employment_statuses,id',
            'role_id' => 'required|exists:roles,id',
        ]);
    }
}
