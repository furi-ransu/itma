<?php

namespace App\Actions\Api\Resource\Branch\Update;

use App\Models\Branch;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Branch::where('id', $request->id)->update($request->only(['name', 'city_id']));
    }
}
