<?php

namespace App\Actions\Api\Resource\Branch\Update;

use Illuminate\Validation\Rule;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:branches,id',
            'name' => [
                'required',
                'string',
                'max:255',
                Rule::unique('branches')->ignore($request->id),
            ],
            'city_id' => 'required|exists:cities,id',
        ]);
    }
}
