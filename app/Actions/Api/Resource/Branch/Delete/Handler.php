<?php

namespace App\Actions\Api\Resource\Branch\Delete;

class Handler
{
    public function handle(\Illuminate\Http\Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $request->merge(['id' => $id]);
        ValidateRequest::handle($request);
        DeleteData::handle($request);

        return response()->api(200, 'success', [], 'Branch delete successfully');
    }
}
