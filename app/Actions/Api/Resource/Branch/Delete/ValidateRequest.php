<?php

namespace App\Actions\Api\Resource\Branch\Delete;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'id' => 'required|exists:branches,id',
        ]);
    }
}
