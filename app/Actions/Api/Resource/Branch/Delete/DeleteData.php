<?php

namespace App\Actions\Api\Resource\Branch\Delete;

use App\Models\Branch;

class DeleteData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Branch::where('id', $request->id)->delete();
    }
}
