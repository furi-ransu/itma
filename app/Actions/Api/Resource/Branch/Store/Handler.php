<?php

namespace App\Actions\Api\Resource\Branch\Store;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        ValidateRequest::handle($request);
        SaveData::handle($request);

        return response()->api(200, 'success', [], 'Branch created successfully');
    }
}
