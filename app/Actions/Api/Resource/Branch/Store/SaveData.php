<?php

namespace App\Actions\Api\Resource\Branch\Store;

use App\Models\Branch;

class SaveData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        Branch::create($request->only(['name', 'city_id']));
    }
}
