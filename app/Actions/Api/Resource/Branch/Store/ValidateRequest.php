<?php

namespace App\Actions\Api\Resource\Branch\Store;

class ValidateRequest
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:100|unique:branches,name',
            'city_id' => 'required|exists:cities,id',
        ]);
    }
}
