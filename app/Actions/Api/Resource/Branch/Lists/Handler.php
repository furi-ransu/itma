<?php

namespace App\Actions\Api\Resource\Branch\Lists;

use App\Models\Branch;
use Yajra\DataTables\Facades\DataTables;

class Handler
{
    public function handle(\Illuminate\Http\Request $request)
    {
        $model = Branch::query()->with(['city.state.country'])->select('branches.*');

        return DataTables::eloquent($model)
            ->addColumn('actions', function ($row) {
                return view('cms.branch.lists.includes.column-action.branch-datatable', [
                    'route_edit' => route('cms.branches.edit', $row->id),
                    'route_delete' => route('api.resource.branch.delete', $row->id),
                ]);
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
