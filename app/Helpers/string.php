<?php

if (! function_exists('isJson')) {
    function isJson(string $str): bool
    {
        json_decode($str);

        return json_last_error() == JSON_ERROR_NONE;
    }
}
