<?php

use App\Models\Setting;

if (! function_exists('convertToUsd')) {
    function convertToUsd($currency, $nominal)
    {
        $result = 0;

        if ($currency == 'IDR') {
            $setting = Setting::where('key', 'idr_to_usd')->first();
            $result = ($nominal * $setting->value);
        } elseif ($currency == 'SGD') {
            $setting = Setting::where('key', 'sgd_to_usd')->first();
            $result = ($nominal * $setting->value);
        } else {
            $setting = Setting::where('key', 'usd_to_usd')->first();
            $result = ($nominal * $setting->value);
        }

        return $result;
    }
}
