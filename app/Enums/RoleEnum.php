<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

final class RoleEnum extends Enum
{
    private const BOD = 'bod';

    private const STAFF = 'staff';

    private const DEPARTMENTMANAGER = 'department manager';

    private const SENIORMANAGER = 'senior manager';

    private const TEAMLEADER = 'team leader';

    private const ADMIN = 'admin';
}
