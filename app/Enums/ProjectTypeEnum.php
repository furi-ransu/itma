<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

final class ProjectTypeEnum extends Enum
{
    private const NEW = 'new';

    private const RENEWAL = 'renewal';
}
