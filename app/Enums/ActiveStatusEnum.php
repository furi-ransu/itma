<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

final class ActiveStatusEnum extends Enum
{
    private const NO = 'no';

    private const YES = 'yes';
}
