<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Invoice
 *
 * @property int $id
 * @property string $number
 * @property string|null $po_number
 * @property string|null $tax_number
 * @property Carbon|null $due_date
 * @property Carbon $transaction_date
 * @property string $customer_name
 * @property string $customer_number
 * @property string|null $currency_iso_code
 * @property string $status
 * @property float $nominal
 * @property int $age_of_unpaid_invoice
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|Project[] $projects
 */
class Invoice extends Model
{
    protected $table = 'invoices';

    protected $casts = [
        'nominal' => 'float',
        'age_of_unpaid_invoice' => 'int',
    ];

    protected $fillable = [
        'number',
        'po_number',
        'tax_number',
        'due_date',
        'transaction_date',
        'customer_name',
        'customer_number',
        'currency_iso_code',
        'status',
        'nominal',
        'age_of_unpaid_invoice',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
