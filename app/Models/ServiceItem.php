<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceItem
 *
 * @property int $id
 * @property string $name
 * @property int $service_group_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property ServiceGroup $service_group
 * @property Collection|Project[] $projects
 * @property Collection|WorkItem[] $work_items
 */
class ServiceItem extends Model
{
    protected $table = 'service_items';

    protected $casts = [
        'service_group_id' => 'int',
    ];

    protected $fillable = [
        'name',
        'service_group_id',
    ];

    public function service_group()
    {
        return $this->belongsTo(ServiceGroup::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function work_items()
    {
        return $this->hasMany(WorkItem::class);
    }
}
