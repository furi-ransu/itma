<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 *
 * @property int $id
 * @property string $name
 * @property string $active_status
 * @property string $type
 * @property string $email
 * @property string $address
 * @property string $phone_number
 * @property string|null $kyc_url_document
 * @property int $md_kyc_status_id
 * @property int $city_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property City $city
 * @property MdKycStatus $md_kyc_status
 * @property Collection|Project[] $projects
 */
class Client extends Model
{
    protected $table = 'clients';

    protected $casts = [
        'md_kyc_status_id' => 'int',
        'city_id' => 'int',
    ];

    protected $fillable = [
        'name',
        'active_status',
        'type',
        'email',
        'address',
        'phone_number',
        'kyc_url_document',
        'md_kyc_status_id',
        'city_id',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function md_kyc_status()
    {
        return $this->belongsTo(MdKycStatus::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
