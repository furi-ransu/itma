<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Branch
 *
 * @property int $id
 * @property string $name
 * @property int $city_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property City $city
 * @property Collection|Department[] $departments
 */
class Branch extends Model
{
    protected $table = 'branches';

    protected $casts = [
        'city_id' => 'int',
    ];

    protected $fillable = [
        'name',
        'city_id',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'branch_departments')
            ->withPivot('id')
            ->withTimestamps();
    }
}
