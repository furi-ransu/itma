<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 *
 * @property int $id
 * @property string $number
 * @property int $lead_id
 * @property int $client_id
 * @property int $invoice_id
 * @property int $branch_department_id
 * @property int $service_item_id
 * @property int $service_category_id
 * @property int $project_type_id
 * @property int $project_status_id
 * @property int $consultant_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property BranchDepartment $branch_department
 * @property Client $client
 * @property Emp $emp
 * @property Invoice $invoice
 * @property Lead $lead
 * @property ProjectStatus $project_status
 * @property ProjectType $project_type
 * @property ServiceCategory $service_category
 * @property ServiceItem $service_item
 * @property Collection|ProjectCb[] $project_cbs
 */
class Project extends Model
{
    protected $table = 'projects';

    protected $casts = [
        'lead_id' => 'int',
        'client_id' => 'int',
        'invoice_id' => 'int',
        'branch_department_id' => 'int',
        'service_item_id' => 'int',
        'service_category_id' => 'int',
        'project_type_id' => 'int',
        'project_status_id' => 'int',
        'consultant_id' => 'int',
    ];

    protected $fillable = [
        'number',
        'lead_id',
        'client_id',
        'invoice_id',
        'branch_department_id',
        'service_item_id',
        'service_category_id',
        'project_type_id',
        'project_status_id',
        'consultant_id',
    ];

    public function branch_department()
    {
        return $this->belongsTo(BranchDepartment::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function emp()
    {
        return $this->belongsTo(Emp::class, 'consultant_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class);
    }

    public function project_status()
    {
        return $this->belongsTo(ProjectStatus::class);
    }

    public function project_type()
    {
        return $this->belongsTo(ProjectType::class);
    }

    public function service_category()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function service_item()
    {
        return $this->belongsTo(ServiceItem::class);
    }

    public function project_cbs()
    {
        return $this->hasMany(ProjectCb::class);
    }
}
