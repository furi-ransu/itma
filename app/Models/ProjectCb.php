<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectCb
 *
 * @property int $id
 * @property int $project_id
 * @property int $consultant_bonus_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property ConsultantBonus $consultant_bonus
 * @property Project $project
 */
class ProjectCb extends Model
{
    protected $table = 'project_cbs';

    protected $casts = [
        'project_id' => 'int',
        'consultant_bonus_id' => 'int',
    ];

    protected $fillable = [
        'project_id',
        'consultant_bonus_id',
    ];

    public function consultant_bonus()
    {
        return $this->belongsTo(ConsultantBonus::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
