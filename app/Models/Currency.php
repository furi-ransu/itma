<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 *
 * @property int $id
 * @property int $priority
 * @property string $iso_code
 * @property string $name
 * @property string $symbol
 * @property string $subunit
 * @property int $subunit_to_unit
 * @property bool $symbol_first
 * @property string $html_entity
 * @property string $decimal_mark
 * @property string $thousands_separator
 * @property int $iso_numeric
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|LeadCb[] $lead_cbs
 * @property Collection|PicCb[] $pic_cbs
 */
class Currency extends Model
{
    protected $table = 'currencies';

    protected $casts = [
        'priority' => 'int',
        'subunit_to_unit' => 'int',
        'symbol_first' => 'bool',
        'iso_numeric' => 'int',
    ];

    protected $fillable = [
        'priority',
        'iso_code',
        'name',
        'symbol',
        'subunit',
        'subunit_to_unit',
        'symbol_first',
        'html_entity',
        'decimal_mark',
        'thousands_separator',
        'iso_numeric',
    ];

    public function lead_cbs()
    {
        return $this->hasMany(LeadCb::class);
    }

    public function pic_cbs()
    {
        return $this->hasMany(PicCb::class);
    }
}
