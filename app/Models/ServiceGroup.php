<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceGroup
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|ServiceItem[] $service_items
 */
class ServiceGroup extends Model
{
    protected $table = 'service_groups';

    protected $fillable = [
        'name',
    ];

    public function service_items()
    {
        return $this->hasMany(ServiceItem::class);
    }
}
