<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TeamMember
 *
 * @property int $id
 * @property string $active_status
 * @property int $emp_id
 * @property int $team_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Emp $emp
 * @property Team $team
 */
class TeamMember extends Model
{
    protected $table = 'team_members';

    protected $casts = [
        'emp_id' => 'int',
        'team_id' => 'int',
    ];

    protected $fillable = [
        'active_status',
        'emp_id',
        'team_id',
    ];

    public function emp()
    {
        return $this->belongsTo(Emp::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
