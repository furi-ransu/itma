<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Department
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|Branch[] $branches
 */
class Department extends Model
{
    protected $table = 'departments';

    protected $fillable = [
        'name',
    ];

    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'branch_departments')
            ->withPivot('id')
            ->withTimestamps();
    }
}
