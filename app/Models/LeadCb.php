<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LeadCb
 *
 * @property int $id
 * @property float $total_nominal
 * @property int $currency_id
 * @property int $emp_id
 * @property int $consultant_bonus_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property ConsultantBonus $consultant_bonus
 * @property Currency $currency
 * @property Emp $emp
 */
class LeadCb extends Model
{
    protected $table = 'lead_cbs';

    protected $casts = [
        'total_nominal' => 'float',
        'currency_id' => 'int',
        'emp_id' => 'int',
        'consultant_bonus_id' => 'int',
    ];

    protected $fillable = [
        'total_nominal',
        'currency_id',
        'emp_id',
        'consultant_bonus_id',
    ];

    public function consultant_bonus()
    {
        return $this->belongsTo(ConsultantBonus::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function emp()
    {
        return $this->belongsTo(Emp::class);
    }
}
