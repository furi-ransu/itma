<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Lead
 *
 * @property int $id
 * @property string $name
 * @property Carbon $date
 * @property string $description
 * @property string $is_won
 * @property string $odoo_lead_id
 * @property string $odoo_partner_id
 * @property string|null $odoo_partner_name
 * @property string|null $odoo_partner_email
 * @property string|null $odoo_partner_phone_number
 * @property string|null $detail
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|Project[] $projects
 */
class Lead extends Model
{
    protected $table = 'leads';

    protected $fillable = [
        'name',
        'date',
        'description',
        'is_won',
        'odoo_lead_id',
        'odoo_partner_id',
        'odoo_partner_name',
        'odoo_partner_email',
        'odoo_partner_phone_number',
        'detail',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
