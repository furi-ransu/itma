<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BranchDepartment
 *
 * @property int $id
 * @property int $branch_id
 * @property int $department_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Branch $branch
 * @property Department $department
 * @property Collection|Emp[] $emps
 * @property Collection|Project[] $projects
 * @property Collection|Team[] $teams
 */
class BranchDepartment extends Model
{
    protected $table = 'branch_departments';

    protected $casts = [
        'branch_id' => 'int',
        'department_id' => 'int',
    ];

    protected $fillable = [
        'branch_id',
        'department_id',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function emps()
    {
        return $this->hasMany(Emp::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function teams()
    {
        return $this->hasMany(Team::class);
    }
}
