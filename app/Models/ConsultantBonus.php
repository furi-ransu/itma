<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ConsultantBonus
 *
 * @property int $id
 * @property int $month
 * @property int $year
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|LeadCb[] $lead_cbs
 * @property Collection|PicCb[] $pic_cbs
 * @property Collection|ProjectCb[] $project_cbs
 */
class ConsultantBonus extends Model
{
    protected $table = 'consultant_bonuses';

    protected $casts = [
        'month' => 'int',
        'year' => 'int',
    ];

    protected $fillable = [
        'month',
        'year',
    ];

    public function lead_cbs()
    {
        return $this->hasMany(LeadCb::class);
    }

    public function pic_cbs()
    {
        return $this->hasMany(PicCb::class);
    }

    public function project_cbs()
    {
        return $this->hasMany(ProjectCb::class);
    }
}
