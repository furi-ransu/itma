<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MdDeadlineDayType
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class MdDeadlineDayType extends Model
{
    protected $table = 'md_deadline_day_types';

    protected $fillable = [
        'name',
    ];
}
