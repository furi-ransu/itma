<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Presenters\TeamPresenter;
use Carbon\Carbon;
use Coderflex\LaravelPresenter\Concerns\CanPresent;
use Coderflex\LaravelPresenter\Concerns\UsesPresenters;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Team
 *
 * @property int $id
 * @property string $name
 * @property int $branch_department_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property BranchDepartment $branch_department
 * @property Collection|TeamMember[] $team_members
 */
class Team extends Model implements CanPresent
{
    use UsesPresenters;

    protected $presenters = [
        'default' => TeamPresenter::class,
    ];

    protected $table = 'teams';

    protected $casts = [
        'branch_department_id' => 'int',
    ];

    protected $fillable = [
        'name',
        'branch_department_id',
    ];

    public function branch_department()
    {
        return $this->belongsTo(BranchDepartment::class);
    }

    public function team_members()
    {
        return $this->hasMany(TeamMember::class);
    }
}
