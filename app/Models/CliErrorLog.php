<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CliErrorLog
 *
 * @property int $id
 * @property string $signature
 * @property string $error_message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class CliErrorLog extends Model
{
    protected $table = 'cli_error_logs';

    protected $fillable = [
        'signature',
        'error_message',
    ];
}
