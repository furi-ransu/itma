<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserOtp
 *
 * @property int $id
 * @property string $code
 * @property Carbon $expired_at
 * @property Carbon|null $redeem_at
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property User $user
 */
class UserOtp extends Model
{
    protected $table = 'user_otps';

    protected $casts = [
        'expired_at' => 'datetime',
        'redeem_at' => 'datetime',
        'user_id' => 'int',
    ];

    protected $fillable = [
        'code',
        'expired_at',
        'redeem_at',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
