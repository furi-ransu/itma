<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WorkItem
 *
 * @property int $id
 * @property string $pipeline
 * @property int $timeline
 * @property int $order
 * @property int $md_day_type_id
 * @property int $service_item_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property MdDayType $md_day_type
 * @property ServiceItem $service_item
 */
class WorkItem extends Model
{
    protected $table = 'work_items';

    protected $casts = [
        'timeline' => 'int',
        'order' => 'int',
        'md_day_type_id' => 'int',
        'service_item_id' => 'int',
    ];

    protected $fillable = [
        'pipeline',
        'timeline',
        'order',
        'md_day_type_id',
        'service_item_id',
    ];

    public function md_day_type()
    {
        return $this->belongsTo(MdDayType::class);
    }

    public function service_item()
    {
        return $this->belongsTo(ServiceItem::class);
    }
}
