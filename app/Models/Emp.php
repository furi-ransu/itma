<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Emp
 *
 * @property int $id
 * @property string $number
 * @property int $user_id
 * @property int $md_employment_status_id
 * @property int $branch_department_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property BranchDepartment $branch_department
 * @property MdEmploymentStatus $md_employment_status
 * @property User $user
 * @property Collection|LeadCb[] $lead_cbs
 * @property Collection|PicCb[] $pic_cbs
 * @property Collection|Project[] $projects
 * @property Collection|TeamMember[] $team_members
 */
class Emp extends Model
{
    protected $table = 'emps';

    protected $casts = [
        'user_id' => 'int',
        'md_employment_status_id' => 'int',
        'branch_department_id' => 'int',
    ];

    protected $fillable = [
        'number',
        'user_id',
        'md_employment_status_id',
        'branch_department_id',
    ];

    public function branch_department()
    {
        return $this->belongsTo(BranchDepartment::class);
    }

    public function md_employment_status()
    {
        return $this->belongsTo(MdEmploymentStatus::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function lead_cbs()
    {
        return $this->hasMany(LeadCb::class);
    }

    public function pic_cbs()
    {
        return $this->hasMany(PicCb::class, 'team_leader_id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'consultant_id');
    }

    public function team_members()
    {
        return $this->hasMany(TeamMember::class);
    }
}
