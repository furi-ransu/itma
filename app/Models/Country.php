<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 *
 * @property int $id
 * @property string $name
 * @property string $iso3
 * @property string $iso2
 * @property string $phonecode
 * @property string $capital
 * @property string $currency
 * @property string $currency_symbol
 * @property string $tld
 * @property string|null $native
 * @property string $region
 * @property string $subregion
 * @property string $timezones
 * @property string|null $translations
 * @property string $latitude
 * @property string $longitude
 * @property string $emoji
 * @property string $emojiU
 * @property bool $flag
 * @property string|null $wikiDataId
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|City[] $cities
 * @property Collection|State[] $states
 */
class Country extends Model
{
    protected $table = 'countries';

    protected $casts = [
        'flag' => 'bool',
    ];

    protected $fillable = [
        'name',
        'iso3',
        'iso2',
        'phonecode',
        'capital',
        'currency',
        'currency_symbol',
        'tld',
        'native',
        'region',
        'subregion',
        'timezones',
        'translations',
        'latitude',
        'longitude',
        'emoji',
        'emojiU',
        'flag',
        'wikiDataId',
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function states()
    {
        return $this->hasMany(State::class);
    }
}
