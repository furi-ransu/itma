<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MdEmploymentStatus
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|Emp[] $emps
 * @property Collection|PicCb[] $pic_cbs
 */
class MdEmploymentStatus extends Model
{
    protected $table = 'md_employment_statuses';

    protected $fillable = [
        'name',
    ];

    public function emps()
    {
        return $this->hasMany(Emp::class);
    }

    public function pic_cbs()
    {
        return $this->hasMany(PicCb::class);
    }
}
