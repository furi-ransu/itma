<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MdDayType
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|WorkItem[] $work_items
 */
class MdDayType extends Model
{
    protected $table = 'md_day_types';

    protected $fillable = [
        'name',
    ];

    public function work_items()
    {
        return $this->hasMany(WorkItem::class);
    }
}
