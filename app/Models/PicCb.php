<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PicCb
 *
 * @property int $id
 * @property float $total_nominal
 * @property int $emp_id
 * @property int $team_leader_id
 * @property int $currency_id
 * @property int $md_employment_status_id
 * @property int $consultant_bonus_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property ConsultantBonus $consultant_bonus
 * @property Currency $currency
 * @property Emp $emp
 * @property MdEmploymentStatus $md_employment_status
 */
class PicCb extends Model
{
    protected $table = 'pic_cbs';

    protected $casts = [
        'total_nominal' => 'float',
        'emp_id' => 'int',
        'team_leader_id' => 'int',
        'currency_id' => 'int',
        'md_employment_status_id' => 'int',
        'consultant_bonus_id' => 'int',
    ];

    protected $fillable = [
        'total_nominal',
        'emp_id',
        'team_leader_id',
        'currency_id',
        'md_employment_status_id',
        'consultant_bonus_id',
    ];

    public function consultant_bonus()
    {
        return $this->belongsTo(ConsultantBonus::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function emp()
    {
        return $this->belongsTo(Emp::class, 'emp_id');
    }

    public function md_employment_status()
    {
        return $this->belongsTo(MdEmploymentStatus::class);
    }
}
