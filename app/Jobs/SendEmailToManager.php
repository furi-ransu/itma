<?php

namespace App\Jobs;

use App\Mail\ProjectNumberMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendEmailToManager implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $number;

    protected $email;

    /**
     * Create a new job instance.
     */
    public function __construct($email, $number)
    {
        $this->number = $number;
        $this->email = $email;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $projectNumberMail = new ProjectNumberMail($this->number);
        Mail::to($this->email)->send($projectNumberMail);
    }
}
