<?php

namespace App\Listeners;

use App\Mail\OtpMail;
use App\Models\UserOtp;
use Illuminate\Auth\Events\Login as IlluminateAuthEventsLogin;
use Illuminate\Support\Facades\Mail;

class LoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(IlluminateAuthEventsLogin $event)
    {
        $user = $event->user;
        $otpCode = rand(100000, 999999); // Generate a 6-digit OTP
        $expiresAt = now()->addMinutes(10); // OTP expires in 10 minutes

        $userOtpExists = UserOtp::where('user_id', $user->id)->latest()->first();
        if ($userOtpExists) {
            $userOtpExists->code = $otpCode;
            $userOtpExists->expired_at = $expiresAt;
            $userOtpExists->redeem_at = null;
            $userOtpExists->save();
        } else {
            UserOtp::create([
                'code' => $otpCode,
                'expired_at' => $expiresAt,
                'user_id' => $user->id,
            ]);
        }

        Mail::to($user->email)->send(new OtpMail($otpCode));
    }
}
