<?php

namespace App\Presenters;

use App\Enums\RoleEnum;
use App\Models\TeamMember;
use Coderflex\LaravelPresenter\Presenter;

class TeamPresenter extends Presenter
{
    public function get_team_leader_id()
    {
        $teamLeaderId = null;
        $teamMembers = TeamMember::with(['emp.user'])->where('team_id', $this->model->id)->get();

        if ($teamMembers->count() > 0) {
            foreach ($teamMembers as $key => $value) {
                $user = $value->emp->user;
                $userRole = $user->getRoleNames()->first();

                if ($userRole == RoleEnum::TEAMLEADER()->getValue()) {
                    $teamLeaderId = $user->id;
                    break;
                }
            }
        }

        return $teamLeaderId;
    }
}
