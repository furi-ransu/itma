<?php

namespace App\Http\Middleware;

use App\Enums\RoleEnum;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiOnlyAdminCanBeAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $request->validate([
            'user_id_logged_in' => 'required|exists:users,id',
        ]);
        $user = \App\Models\User::find($request->user_id_logged_in);
        $userRole = $user->getRoleNames()->first();

        if ($userRole != RoleEnum::ADMIN()->getValue()) {
            throw new \Exception('Only admin can be access');
        }

        return $next($request);
    }
}
