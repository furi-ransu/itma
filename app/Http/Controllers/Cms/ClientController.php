<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('cms.client.lists.main');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $types = ['individual', 'corporate'];
        $kycStatuses = \App\Models\MdKycStatus::get();

        return view('cms.client.create.main', compact('kycStatuses', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $activeStatuses = ['yes', 'no'];
        $types = ['individual', 'corporate'];
        $kycStatuses = \App\Models\MdKycStatus::get();
        $client = \App\Models\Client::with(['md_kyc_status', 'city'])->find($id);

        return view('cms.client.edit.main', compact('id', 'client', 'kycStatuses', 'types', 'activeStatuses'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
