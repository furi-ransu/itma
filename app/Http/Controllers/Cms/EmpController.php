<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmpController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('cms.emp.lists.main');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $roles = \App\Models\Role::get();
        $mdEmploymentStatuses = \App\Models\MdEmploymentStatus::get();

        return view('cms.emp.create.main', compact('roles', 'mdEmploymentStatuses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $activeStatuses = ['no', 'yes'];
        $emp = \App\Models\Emp::with([
            'user',
            'branch_department.branch',
            'branch_department.department',
            'md_employment_status',
        ])->find($id);
        $roles = \App\Models\Role::get();
        $mdEmploymentStatuses = \App\Models\MdEmploymentStatus::get();
        $userRole = \App\Models\User::find($emp->user_id)->getRoleNames()->first();

        return view('cms.emp.edit.main', compact('id', 'emp', 'roles', 'mdEmploymentStatuses', 'userRole', 'activeStatuses'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
