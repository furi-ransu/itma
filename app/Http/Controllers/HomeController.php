<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        $lastOtp = \App\Models\UserOtp::where('user_id', $user->id)->whereNull('redeem_at')->first();
        if ($lastOtp) {
            return redirect()->route('otp-verification.email', ['email' => $user->email]);
        }

        return view('home');
    }
}
