<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserOtp;
use Illuminate\Http\Request;

class OtpVerificationController extends Controller
{
    public function email(string $email)
    {
        if (! $email) {
            return redirect()->route('login');
        }

        $user = User::where('email', $email)->first();
        $otpExists = UserOtp::where('user_id', $user->id)->whereNull('redeem_at')->latest();

        if ($otpExists->exists()) {
            return view('auth.verify-otp', ['email' => $email]);
        }

        return redirect()->route('login');
    }

    public function verify(Request $request)
    {
        try {
            app('app.action.webui.otp.verify')->handle($request);

            return redirect()->route('home');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }
}
